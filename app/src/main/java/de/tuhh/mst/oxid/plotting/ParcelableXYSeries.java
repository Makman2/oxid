package de.tuhh.mst.oxid.plotting;

import android.os.Parcelable;

import com.androidplot.xy.XYSeries;


public abstract class ParcelableXYSeries implements XYSeries, Parcelable {}
