package de.tuhh.mst.oxid.hardware;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import static de.tuhh.mst.oxid.utilities.threading.joinThread;


/**
 * Base class for playing an arbitrary long audio signal.
 */
public abstract class AudioOutputGeneratorBase {
    /**
     * Instantiates a new {@link AudioOutputGeneratorBase} without a listener.
     *
     * You need to call {@link #setOnSignalQueriedListener(OnSignalQueriedListener)} until you can
     * start playing audio.
     *
     * @param sampling_rate The sampling frequency of the output signal.
     */
    public AudioOutputGeneratorBase(int sampling_rate) {
        this(sampling_rate, null);
    }

    /**
     * Instantiates a new {@link AudioOutputGeneratorBase}.
     *
     * @param sampling_rate The sampling frequency of the output signal.
     * @param listener      The {@link OnSignalReceivedListener} that is triggered when a chunk of
     *                      the signal to play is requested.
     */
    public AudioOutputGeneratorBase(int sampling_rate, OnSignalQueriedListener listener) {
        this.sampling_rate = sampling_rate;
        _listener = listener;
        // Taking the buffer size x16, x8 for performance reasons and implicitly x2 because the size
        // of buffer_size of this class is denoted in shorts.
        this.buffer_size = 8 * AudioTrack.getMinBufferSize(sampling_rate,
                                                           AudioFormat.CHANNEL_OUT_MONO,
                                                           AudioFormat.ENCODING_PCM_16BIT);

        initializeAudioTrack();
    }

    private void initializeAudioTrack() {
        // 16-bit PCM coding is guaranteed to work on every device:
        // https://developer.android.com/reference/android/media/AudioFormat.html#ENCODING_PCM_16BIT
        _audio_track = new AudioTrack(AudioManager.STREAM_MUSIC,
            sampling_rate,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT,
            buffer_size * (Short.SIZE / Byte.SIZE),
            AudioTrack.MODE_STREAM);

        if (_audio_track.getState() != AudioTrack.STATE_INITIALIZED) {
            throw new RuntimeException("Underlying AudioTrack wasn't successfully initialized.");
        }
    }

    /**
     * Sets the listener that is queried for buffer chunks that shall be played as audio.
     *
     * You must set a listener before you start the recording via {@link #start()}.
     *
     * @param listener               The listener to set.
     * @throws IllegalStateException Thrown when currently playing.
     */
    protected void setOnSignalQueriedListener(OnSignalQueriedListener listener) {
        if (_is_running.get()) {
            throw new IllegalStateException("Can't set listener while playing.");
        }

        _listener = listener;
    }

    /**
     * Gets the listener that is queried for buffer chunks that shall be played as audio.
     *
     * If no listener is set, this function returns @{code null}.
     *
     * @return The listener currently set.
     */
    protected OnSignalQueriedListener getOnSignalQueriedListener() {
        return _listener;
    }

    /**
     * Starts the playback.
     *
     * This function blocks until the internal playback buffer is filled up with data, so that the
     * playback does not interrupt because of an under-run state.
     *
     * If the playback is already started, this function has no effect.
     *
     * Note that the actual playback probably starts a bit later after calling this function. This
     * depends on the device and the execution overhead involved to get the output ready.
     *
     * @throws IllegalStateException Thrown when no {@link OnSignalReceivedListener} is set.
     */
    public void start() {
        if (_listener == null) {
            throw new IllegalStateException("Can't start playing audio when no listener set via " +
                                            "getOnSignalReceivedListener().");
        }

        if (!_is_running.get()) {
            // Already fill up the buffer to allow immediate and uninterrupted playback.
            short[] signal = {};
            int write_count = 0;
            int written = 0;
            while (written < buffer_size) {
                signal = _listener.get_signal();

                write_count = Math.min(signal.length, buffer_size - written);

                _audio_track.write(signal, 0, write_count);

                written += write_count;
            }

            final short[] remaining_signal = Arrays.copyOfRange(signal, write_count, signal.length);

            _buffer_write_thread = new Thread() {
                @Override
                public void run() {
                    if (_is_running.get()) {
                        // Fill up the remaining samples from the initial buffer fill.
                        if (remaining_signal.length > 0) {
                            _audio_track.write(remaining_signal, 0, remaining_signal.length);
                        }
                    }
                    else {
                        return;
                    }

                    while (_is_running.get()) {
                        short[] signal = _listener.get_signal();
                        _audio_track.write(signal, 0, signal.length);
                    }
                }
            };

            // Start continuous playback.
            _audio_track.play();
            _is_running.set(true);
            _buffer_write_thread.start();
        }
    }

    /**
     * Stops the audio output.
     *
     * Note that remaining writes to the audio buffer in process are still completed and played.
     * Until this completes, this function blocks.
     *
     * Invoking {@link #start()} is possible after calling {@link #stop()}.
     */
    public void stop() {
        _is_running.set(false);
        _audio_track.stop();

        if (_buffer_write_thread != null) {
            joinThread(_buffer_write_thread);
            _buffer_write_thread = null;
        }
    }

    /**
     * Stops playing and releases all resources occupied from the underlying {@link AudioTrack}.
     */
    public void release() {
        stop();

        if (_audio_track != null) {
            _audio_track.release();
        }
    }

    /**
     * The sampling frequency of the output signal.
     */
    public final int sampling_rate;

    /**
     * The buffer size of the internal buffer (in shorts) used for streaming audio. This is a
     * multiple of 16 of the minimum required size for the audio buffer.
     *
     * You can use this value to optimize the loading of the audio buffer via
     * {@link OnSignalQueriedListener#get_signal()}.
     */
    public final int buffer_size;

    private AudioTrack _audio_track;
    private OnSignalQueriedListener _listener;
    // Because it's not documented how immediately the AudioTrack.getPlayState() changes after a
    // call to AudioTrack.stop(), we need to synchronize that ourselves.
    private final AtomicBoolean _is_running = new AtomicBoolean(false);
    private Thread _buffer_write_thread;
}
