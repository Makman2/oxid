package de.tuhh.mst.oxid.ui;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.util.OpenIntToDoubleHashMap;

import java.nio.DoubleBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.hardware.AudioOutputGeneratorBase;
import de.tuhh.mst.oxid.hardware.AudioRecorderProcessor;
import de.tuhh.mst.oxid.hardware.OnSignalReceivedListener;
import de.tuhh.mst.oxid.hardware.SineAudioOutputGenerator;
import de.tuhh.mst.oxid.plotting.EquidistantYSeries;
import de.tuhh.mst.oxid.utilities.arrays;
import de.tuhh.mst.oxid.utilities.math;


/**
 * Presents sample audio FFT processing.
 */
public class FFTActivityFragment extends Fragment {

    public FFTActivityFragment()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fft, container, false);

        _ui_plot = (XYPlot)view.findViewById(R.id.fragment_fft_plot);
        _ui_plot.getLegend().setVisible(false);
        _ui_plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.LEFT).setFormat(new DecimalFormat("#"));
        _ui_plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new DecimalFormat("#"));
        _ui_plot.setPlotMargins(0.0f, 0.0f, 0.0f, 0.0f);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        _plot_series_formatter = new LineAndPointFormatter();
        _plot_series_formatter.setVertexPaint(null);
        _plot_series_formatter.getLinePaint().setColor(0xFFAA00AA);
        _plot_series_formatter.getFillPaint().setColor(0x00000000);
    }

    private class ProcessSignalAsyncTask extends AsyncTask<short[], Void, XYSeries> {
        public ProcessSignalAsyncTask(double smoothing_factor) {
            if (smoothing_factor == 0.0) {
                _smooth_kernel = math.compute_identity_smooth_kernel();
            }
            else {
                _smooth_kernel = math.compute_gauss_smooth_kernel(smoothing_factor);
            }
        }

        @Override
        protected XYSeries doInBackground(short[]... signals) {
            if (signals.length > 1) {
                Log.w(ProcessSignalAsyncTask.class.getName() + ".doInBackground()",
                      "Only 1 signal supported, but passed " + signals.length + ". Ignoring " +
                          "additional signals.");
            }

            return process_signal(signals[0]);
        }

        protected XYSeries process_signal(short[] signal) {
            Complex[] transformed = math.dft(arrays.toDouble(signal));

            // Only use the lower half, as we have a symmetric signal + Nyquist
            // frequency.
            transformed = Arrays.copyOfRange(transformed, 0, transformed.length / 2 + 1);

            // TODO Compute and present both: abs and arg

            // Use just the absolute value to get the frequency parts.
            DoubleBuffer data = DoubleBuffer.allocate(transformed.length);

            for (Complex y : transformed) {
                // Normalize to real units and double up as we receive a symmetric
                // signal and take only the first half.
                data.put(y.abs() * 2.0 / (double)transformed.length);

                // TODO Map to volt(seconds)
            }

            // Apply a gaussian smooth kernel.
            ArrayList<Number> plotdata = new ArrayList<>(transformed.length);
            for (double y : math.smooth(data.array(), _smooth_kernel)) {
                plotdata.add(y);
            }

            EquidistantYSeries series = new EquidistantYSeries(
                plotdata, (double)(_recorder.sampling_rate / 2) / (plotdata.size() - 1), "");

            return series;
        }

        @Override
        protected void onPostExecute(XYSeries series) {
            // TODO Implement phase angle subtraction
            _ui_plot.clear();
            _ui_plot.addSeries(series, _plot_series_formatter);
            _ui_plot.redraw();
        }

        private OpenIntToDoubleHashMap _smooth_kernel;
    }

    @Override
    public void onResume() {
        super.onResume();

        Resources resources = getResources();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        final int input_sampling_rate = Integer.parseInt(preferences.getString(
            resources.getString(R.string.preference_fft_input_sampling_rate_key),
            resources.getString(R.string.preference_fft_input_sampling_rate_default)));

        final double smoothing_factor = Float.parseFloat(preferences.getString(
            resources.getString(R.string.preference_fft_smoothing_factor_key),
            resources.getString(R.string.preference_fft_smoothing_factor_default)));

        _recorder = new AudioRecorderProcessor(input_sampling_rate, new OnSignalReceivedListener() {
            @Override
            public void process_signal(short[] signal) {
                ProcessSignalAsyncTask task = new ProcessSignalAsyncTask(smoothing_factor);
                task.execute(signal);
            }
        });

        String output_signal_type = preferences.getString(
            resources.getString(R.string.preference_fft_output_signal_type_key),
            resources.getString(R.string.preference_fft_output_signal_type_default));

        if (output_signal_type.equals("None")) {
            _signal_generator = null;
        }
        else {
            final int frequency = Integer.parseInt(preferences.getString(
                getString(R.string.preference_fft_output_signal_frequency_key),
                resources.getString(R.string.preference_fft_output_signal_frequency_default)));
            final double amplitude = Double.parseDouble(preferences.getString(
                getString(R.string.preference_fft_output_signal_amplitude_key),
                resources.getString(R.string.preference_fft_output_signal_amplitude_default)));

            if (output_signal_type.equals("Sine")) {
                _signal_generator = new SineAudioOutputGenerator(frequency, amplitude);
            }
            else {
                throw new AssertionError(
                    "Invalid output signal type occurred: " + output_signal_type);
            }

            _signal_generator.start();
        }

        // This statement should be as close as possible to _signal_generator.start() to have
        // reliable start conditions regarding the beginning of recording and output.
        _recorder.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (_signal_generator != null) {
            _signal_generator.stop();
            _signal_generator.release();
        }

        _recorder.stop();
        _recorder.release();
    }

    private AudioRecorderProcessor _recorder;
    private AudioOutputGeneratorBase _signal_generator;

    private XYPlot _ui_plot;

    private LineAndPointFormatter _plot_series_formatter;
}
