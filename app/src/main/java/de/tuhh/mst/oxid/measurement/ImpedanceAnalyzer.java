package de.tuhh.mst.oxid.measurement;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexUtils;

import de.tuhh.mst.oxid.math.estimators.Estimator;
import de.tuhh.mst.oxid.math.estimators.IdentityEstimator;


public class ImpedanceAnalyzer extends VoltageAnalyzer {
    public ImpedanceAnalyzer(double output_voltage,
                             int frequency,
                             int sampling_rate,
                             int sample_size,
                             Complex reference_impedance,
                             double correction_factor,
                             double correction_offset,
                             ImpedanceResultAvailableListener listener) {
        this(output_voltage,
             frequency,
             sampling_rate,
             sample_size,
             20.0,
             reference_impedance,
             correction_factor,
             correction_offset,
             new IdentityEstimator(),
             new IdentityEstimator(),
             listener);
    }

    public ImpedanceAnalyzer(double output_voltage,
                             int frequency,
                             int sampling_rate,
                             int sample_size,
                             double frequency_uncertainty,
                             Complex reference_impedance,
                             double correction_factor,
                             double correction_offset,
                             Estimator absolute_estimator,
                             Estimator argument_estimator,
                             ImpedanceResultAvailableListener listener) {
        super(output_voltage,
              frequency,
              sampling_rate,
              sample_size,
              frequency_uncertainty,
              absolute_estimator,
              argument_estimator);

        _reference_impedance = reference_impedance;
        _correction_factor = correction_factor;
        _correction_offset = correction_offset;
        _output_voltage = new Complex(output_voltage);

        _listener = listener;
    }

    @Override
    protected final void onVoltageAvailable(Complex voltage) {
        Complex corrected_voltage = voltage.add(_correction_offset).multiply(_correction_factor);
        _last_voltage = corrected_voltage;

        Complex impedance = _output_voltage
            .divide(corrected_voltage)
            .subtract(1.0)
            .multiply(_reference_impedance);

        _listener.onImpedanceResultAvailable(impedance);
    }

    /**
     * Synchronizes the input and output signals by equalizing the phase of the output signal
     * with the measured input signal.
     */
    public void calibrate() {
        double calibration_angle = _last_voltage.divide(_output_voltage).getArgument();

        _output_voltage = _output_voltage.multiply(
            ComplexUtils.polar2Complex(1.0, calibration_angle));
    }

    private Complex _reference_impedance;
    private double _correction_factor;
    private double _correction_offset;
    private Complex _output_voltage;
    private Complex _last_voltage;

    private ImpedanceResultAvailableListener _listener;
}
