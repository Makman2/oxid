package de.tuhh.mst.oxid.plotting;

import com.androidplot.xy.XYSeries;

import java.util.ArrayList;
import java.util.List;


public class RunningSeries implements XYSeries {
    public RunningSeries(String title) {
        _title = title;
        _data = new ArrayList<>();
    }

    @Override
    public int size() {
        return _data.size();
    }

    @Override
    public Number getX(int index) {
        return index;
    }

    @Override
    public Number getY(int index) {
        return _data.get(index);
    }

    @Override
    public String getTitle() {
        return _title;
    }

    public void add(Number value) {
        _data.add(value);
    }

    private String _title;
    private List<Number> _data;
}
