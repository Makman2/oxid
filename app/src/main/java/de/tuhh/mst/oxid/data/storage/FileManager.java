package de.tuhh.mst.oxid.data.storage;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Manages publicly available files on external storage.
 */
public class FileManager {
    /**
     * Creates a new {@link FileManager} working inside project root.
     */
    public FileManager() {
        this(null);
    }

    /**
     * Creates a new {@link FileManager} working inside a specified subdirectory in project root.
     *
     * @param subdirectory The subdirectory inside the project root where files shall be managed.
     */
    public FileManager(String subdirectory) {
        _subdirectory = subdirectory;

        getDirectory().mkdirs();
    }

    /**
     * Creates a new file and opens a stream for writing. Existing files will be overridden.
     *
     * @param filename The filename to create.
     * @return         An opened output stream to the file.
     */
    public FileOutputStream create(String filename) throws IOException {
        return new FileOutputStream(getFile(filename));
    }

    /**
     * Creates a new file and opens a stream for writing.
     *
     * If the file does exist already, an {@link IOException} is thrown.
     *
     * @param filename     The filename to create.
     * @return             An opened output stream to the newly created file.
     * @throws IOException Thrown when the file already exists.
     */
    public FileOutputStream createNew(String filename) throws IOException {
        File file = getFile(filename);

        if (!file.createNewFile()) {
            throw new IOException("File already exists.");
        }

        return new FileOutputStream(file);
    }

    /**
     * Deletes a file.
     *
     * @param filename     The filename to delete.
     * @throws IOException Thrown when the file couldn't be deleted.
     */
    public void delete(String filename) throws IOException {
        File file = getFile(filename);
        if (!file.delete()) {
            throw new IOException(filename + " could not be deleted.");
        }
    }

    /**
     * Opens a file for reading.
     *
     * @param filename     The filename to read from.
     * @return             An opened input stream.
     */
    public FileInputStream open(String filename) throws IOException {
        return new FileInputStream(getFile(filename));
    }

    /**
     * Returns the directory where files are stored and can be stored.
     *
     * @return The {@link File} object that describes the working directory.
     */
    public File getDirectory() {
        File external_storage_directory = Environment.getExternalStorageDirectory();
        File oxid_directory = new File(external_storage_directory, "oxid");
        File final_directory = _subdirectory == null ?
                               oxid_directory :
                               new File(oxid_directory, _subdirectory);

        return final_directory;
    }

    /**
     * Returns a {@link File} object created from a filename. The file object will reside in
     * the specific directory of this {@link FileManager}.
     *
     * @param filename The filename.
     * @return         A {@link File} descriptor object describing {@code filename}.
     */
    public File getFile(String filename) {
        return new File(getDirectory(), filename);
    }

    private String _subdirectory;
}
