package de.tuhh.mst.oxid.math.estimators;


/**
 * The {@link CutoffEstimator} is a base class for estimators that resets any user-defined
 * base-estimator as soon as certain conditions are reached.
 *
 * The conditions when the base-estimator is reset are supplied by {@link #needs_reset(double)}.
 */
public abstract class CutoffEstimator extends Estimator {
    public CutoffEstimator(Estimator base_estimator) {
        _base_estimator = base_estimator;
    }

    /**
     * Defines when a reset needs to be triggered.
     *
     * @param value The new value put into this estimator.
     * @return      {@code true} if a reset shall be performed, {@code false} if not.
     */
    protected abstract boolean needs_reset(double value);

    @Override
    public void put(double value) {
        if (needs_reset(value)) {
            _base_estimator.reset();
        }
        _base_estimator.put(value);
    }

    @Override
    public double estimate() {
        return _base_estimator.estimate();
    }

    @Override
    public void reset() {
        _base_estimator.reset();
    }

    /**
     * @return The underlying base-estimator.
     */
    public Estimator getBaseEstimator() {
        return _base_estimator;
    }

    private Estimator _base_estimator;
}
