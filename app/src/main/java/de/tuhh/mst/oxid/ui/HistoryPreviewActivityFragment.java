package de.tuhh.mst.oxid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;

import org.apache.commons.math3.complex.Complex;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.ComplexDataTable;
import de.tuhh.mst.oxid.data.storage.ImpedanceDataTableFileManager;
import de.tuhh.mst.oxid.data.storage.VoltageDataTableFileManager;
import de.tuhh.mst.oxid.ui.viewadapters.HistoryFileAdapterItemType;


public class HistoryPreviewActivityFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_preview, container, false);

        _plot = (XYPlot)view.findViewById(R.id.fragment_history_preview_plot);
        _plot.getLegend().setVisible(false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LineAndPointFormatter formatter = new LineAndPointFormatter();
        formatter.setVertexPaint(null);
        formatter.getLinePaint().setColor(0xFFAA00AA);
        formatter.getFillPaint().setColor(0x00000000);

        // Load data.
        Intent load_intent = getActivity().getIntent();
        File file = new File(
            load_intent.getStringExtra(IntentExtra.history_preview_activity_filename));
        HistoryFileAdapterItemType filetype = HistoryFileAdapterItemType.valueOf(
            load_intent.getStringExtra(IntentExtra.history_preview_activity_filetype));

        ComplexDataTable data_table;
        String plot_title;
        try {
            switch (filetype) {
                case VOLTAGE_MEASUREMENT:
                    data_table = VoltageDataTableFileManager.load(file.getName());
                    plot_title = "Voltage";
                    break;
                case IMPEDANCE_MEASUREMENT:
                    data_table = ImpedanceDataTableFileManager.load(file.getName());
                    plot_title = "Impedance";
                    break;
                default:
                    throw new IllegalArgumentException(
                        "Illegal HistoryFileAdapterItemType value: " + filetype);
            }
        }
        catch (IOException ex) {
            Log.e("oxid", "Can't load '" + file.getName() + "'.", ex);
            Toast.makeText(getContext(), "Failed to load file.", Toast.LENGTH_SHORT).show();

            data_table = new ComplexDataTable(new ArrayList<Complex>());
            plot_title = "";
        }

        // Only plot absolute value.
        List<Number> abs_data = new ArrayList<>(data_table.data.size());
        for (Complex y : data_table.data) {
            abs_data.add(y.abs());
        }

        _plot.setTitle(plot_title);
        // Use the plot title also as series title.
        SimpleXYSeries series = new SimpleXYSeries(
            abs_data, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, plot_title);

        _plot.addSeries(series, formatter);

        // Show 20% extra space at top so one can see still the curve.
        double abs_data_max = abs_data.get(0).doubleValue();
        for (Number v : abs_data) {
            double v_as_double = v.doubleValue();
            if (v_as_double > abs_data_max) {
                abs_data_max = v_as_double;
            }
        }
        _plot.setRangeBoundaries(0, abs_data_max * 1.25, BoundaryMode.FIXED);
    }

    private XYPlot _plot;
}
