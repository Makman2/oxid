package de.tuhh.mst.oxid.plotting;

import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.XYPlot;


public class RunningSeriesTracker {
    public RunningSeriesTracker(XYPlot plot, RunningSeries series, int display_range) {
        this(plot, series, display_range, 0.2);
    }

    public RunningSeriesTracker(XYPlot plot,
                                RunningSeries series,
                                int display_range,
                                double upper_free_space_share) {
        _plot = plot;
        _series = series;
        _display_range = display_range;
        _upper_free_space_share = upper_free_space_share;
    }

    private double computeDisplayedSeriesMax() {
        double last_max = -Double.MAX_VALUE;
        for (int i = Math.max(0, _series.size() - _display_range); i < _series.size(); i++) {
            double val = _series.getY(i).doubleValue();

            if (val > last_max) {
                last_max = val;
            }
        }

        return last_max;
    }

    public void updateBoundaries() {
        int last_display_index = Math.max(_series.size(), _display_range);

        _plot.setDomainBoundaries(
            last_display_index - _display_range,
            last_display_index,
            BoundaryMode.FIXED);

        double upper_range_boundary =
            computeDisplayedSeriesMax() * (1.0 + _upper_free_space_share);

        _plot.setRangeBoundaries(
            0,
            upper_range_boundary,
            BoundaryMode.FIXED);
    }

    private final RunningSeries _series;
    private final XYPlot _plot;
    private final int _display_range;
    private final double _upper_free_space_share;
}
