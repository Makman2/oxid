package de.tuhh.mst.oxid.ui.viewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.math3.util.Pair;

import java.io.File;
import java.util.List;
import java.util.Locale;

import de.tuhh.mst.oxid.R;


/**
 * The adapter that displays a saved data record.
 */
public class HistoryFileAdapter extends ArrayAdapter<Pair<File, HistoryFileAdapterItemType>>
{
    /**
     * Instantiates a new {@link HistoryFileAdapter}.
     *
     * @param context The context to operate on.
     */
    public HistoryFileAdapter(Context context)
    {
        super(context, R.layout.history_file_adapter_item);
    }

    /**
     * Instantiates a new {@link HistoryFileAdapter}.
     *
     * @param context The context to operate on.
     * @param values  The data that should be displayed as list items.
     */
    public HistoryFileAdapter(Context context, List<Pair<File, HistoryFileAdapterItemType>> values)
    {
        super(context, R.layout.history_file_adapter_item, values);
    }

    // Inherited documentation.
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Pair<File, HistoryFileAdapterItemType> item = getItem(position);
        File file = item.getKey();
        HistoryFileAdapterItemType file_type = item.getValue();
        String present_text = file.getName();

        String size_text;
        if (file.length() == 0) {
            // It's likely that an IO error occurred, as we won't store empty files.
            size_text = getContext().getResources().getString(
                R.string.HistoryFileAdapterItem_unknown_size);
        }
        else {
            size_text = String.format(Locale.getDefault(), "%d Bytes", file.length());
        }

        View view;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.history_file_adapter_item, parent, false);
        }
        else {
            view = convertView;
        }

        // Associate UI elements.
        ImageView image = (ImageView)view.findViewById(
            R.id.controls_adapters_history_file_adapter_item_image);
        TextView timestamp = (TextView)view.findViewById(
            R.id.controls_adapters_history_file_adapter_item_timestamp);
        TextView size = (TextView)view.findViewById(
            R.id.controls_adapters_history_file_adapter_item_file_size);

        // Set display properties.
        int image_resource;
        switch (file_type) {
            case VOLTAGE_MEASUREMENT:
                image_resource = R.drawable.voltage_measurement;
                break;
            case IMPEDANCE_MEASUREMENT:
                image_resource = R.drawable.impedance_measurement;
                break;
            default:
                throw new IllegalArgumentException("Illegal file-type provided: " + file_type);
        }
        image.setImageResource(image_resource);
        timestamp.setText(present_text);
        size.setText(size_text);

        return view;
    }
}
