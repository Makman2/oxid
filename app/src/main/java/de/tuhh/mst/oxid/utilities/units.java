package de.tuhh.mst.oxid.utilities;

import org.apache.commons.math3.util.Pair;

import java.util.Locale;


/**
 * Module that contains various helper functions regarding converting and printing physical units
 * and unit prefixes.
 */
public final class units {
    private units() {}

    public enum MetricPrefix {
        FEMTO, PICO, NANO, MICRO, MILLI, NONE, KILO, MEGA, GIGA, TERRA;

        /**
         * Returns the symbol belonging to the metric prefix.
         *
         * @return The symbol as a string.
         */
        public String toSymbolString() {
            switch (this) {
                case FEMTO:
                    return "f";
                case PICO:
                    return "p";
                case NANO:
                    return "n";
                case MICRO:
                    return "μ";
                case MILLI:
                    return "m";
                case NONE:
                    return "";
                case KILO:
                    return "k";
                case MEGA:
                    return "M";
                case GIGA:
                    return "G";
                case TERRA:
                    return "T";
                default:
                    throw new IllegalArgumentException("Unknown prefix: " + this.ordinal());
            }
        }

        /**
         * Returns the multiplication factor denoted by the metric prefix.
         *
         * @return The multiplication factor.
         */
        public double getMultiplier() {
            switch (this) {
                case FEMTO:
                    return 1E-15;
                case PICO:
                    return 1E-12;
                case NANO:
                    return 1E-9;
                case MICRO:
                    return 1E-6;
                case MILLI:
                    return 1E-3;
                case NONE:
                    return 1E+0;
                case KILO:
                    return 1E+3;
                case MEGA:
                    return 1E+6;
                case GIGA:
                    return 1E+9;
                case TERRA:
                    return 1E+12;
                default:
                    throw new IllegalArgumentException("Unknown prefix: " + this.ordinal());
            }
        }
    }

    /**
     * Converts the given amount to an amount matching the given unit prefix.
     *
     * Examples:
     *
     * {@code
     *     convertToMetricPrefix(22000, MetricPrefix.KILO)
     *     // 22
     *     convertToMetricPrefix(0.047, MetricPrefix.MILLI)
     *     // 47
     *     convertToMetricPrefix(0.00003, MetricPrefix.MILLI)
     *     // 0.03
     * }
     *
     * @param amount The amount to convert to a metric prefix.
     * @param prefix The metric prefix to convert to.
     * @return       The converted amount.
     */
    public static double convertToMetricPrefix(double amount, MetricPrefix prefix) {
        return amount / prefix.getMultiplier();
    }

    /**
     * Scales the given number to the nearest unit prefix.
     *
     * The minimum supported unit prefix is {@code f} (femto, 1E-15) and the maximum supported one
     * is {@code T} (Terra, 1E+12).
     *
     * Examples:
     *
     * {@code
     *     convertToNearestMetricPrefix(1000)
     *     // <1, MetricPrefix.KILO>
     *     convertToNearestMetricPrefix(25300000)
     *     // <25.3, MetricPrefix.MEGA>
     *     convertToNearestMetricPrefix(0.07)
     *     // <70, MetricPrefix.MILLI>
     *     convertToNearestMetricPrefix(300E-9)
     *     // <300, MetricPrefix.NANO>
     *     convertToNearestMetricPrefix(50)
     *     // <50, MetricPrefix.NONE>
     * }
     *
     * @param amount The amount to get the metric prefix for and that shall be scaled properly to
     *               this prefix.
     * @return       A pair with {@code <scaled-amount>, <metric-prefix>}
     */
    public static Pair<Double, MetricPrefix> convertToNearestMetricPrefix(double amount) {
        MetricPrefix prefix;

        double absolute_amount = Math.abs(amount);
        if (absolute_amount < 1e-12) {
            prefix = MetricPrefix.FEMTO;
        }
        else if (absolute_amount < 1e-9) {
            prefix = MetricPrefix.PICO;
        }
        else if (absolute_amount < 1e-6) {
            prefix = MetricPrefix.NANO;
        }
        else if (absolute_amount < 1e-3) {
            prefix = MetricPrefix.MICRO;
        }
        else if (absolute_amount < 1.0) {
            prefix = MetricPrefix.MILLI;
        }
        else if (absolute_amount < 1e+3) {
            prefix = MetricPrefix.NONE;
        }
        else if (absolute_amount < 1e+6) {
            prefix = MetricPrefix.KILO;
        }
        else if (absolute_amount < 1e+9) {
            prefix = MetricPrefix.MEGA;
        }
        else if (absolute_amount < 1e+12) {
            prefix = MetricPrefix.GIGA;
        }
        else {
            prefix = MetricPrefix.TERRA;
        }

        return new Pair<>(convertToMetricPrefix(amount, prefix), prefix);
    }

    /**
     * Returns a quantity with a specified unit.
     *
     * @param amount The amount to display.
     * @param digits The number of digits of the amount.
     * @param prefix The metric prefix the amount shall be scaled to.
     * @param unit   The unit (e.g. V for Volts, A for Ampere, etc.).
     * @return       A string showing the unit amount.
     */
    public static String formatUnitString(double amount, int digits, MetricPrefix prefix, String unit) {
        double scaled_amount = convertToMetricPrefix(amount, prefix);
        return String.format(
            Locale.ENGLISH,
            "%." + digits + "f%s%s",
            scaled_amount, prefix.toSymbolString(), unit);
    }

    /**
     * Returns a quantity with a specified unit, without digits behind the comma.
     *
     * @param amount The amount to display.
     * @param prefix The metric prefix the amount shall be scaled to.
     * @param unit   The unit (e.g. V for Volts, A for Ampere, etc.).
     * @return       A string showing the unit amount.
     */
    public static String formatUnitString(double amount, MetricPrefix prefix, String unit) {
        return formatUnitString(amount, 0, prefix, unit);
    }

    /**
     * Returns a quantity with a unit, scaled to the nearest unit prefix.
     *
     * @param amount The amount to display.
     * @param digits The number of digits behind the comma to display.
     * @param unit   The unit (e.g. V for Volts, A for Ampere, etc.).
     * @return       A string showing the unit amount.
     */
    public static String formatUnitString(double amount, int digits, String unit) {
        Pair<Double, MetricPrefix> scaled_amount = convertToNearestMetricPrefix(amount);
        return formatUnitString(amount, digits, scaled_amount.getValue(), unit);
    }

    /**
     * Returns a quantity with a unit, scaled to the nearest unit prefix without digits behind comma.
     *
     * @param amount The amount to display.
     * @param unit   The unit (e.g. V for Volts, A for Ampere, etc.).
     * @return       A string showing the unit amount.
     */
    public static String formatUnitString(double amount, String unit) {
        return formatUnitString(amount, 0, unit);
    }
}
