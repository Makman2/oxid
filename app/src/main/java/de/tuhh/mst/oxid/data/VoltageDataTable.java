package de.tuhh.mst.oxid.data;

import org.apache.commons.math3.complex.Complex;

import java.util.List;


public class VoltageDataTable extends ComplexDataTable {
    public VoltageDataTable(List<Complex> data) {
        super(data);

        sampling_rate = null;
        frequency = null;
        amplitude = null;
        sample_size_in_ms = null;
        frequency_uncertainty = null;
        mean_filter_size_in_ms = null;
    }

    public Integer sampling_rate;
    public Integer frequency;
    public Double amplitude;
    public Double sample_size_in_ms;
    public Double frequency_uncertainty;
    public Double mean_filter_size_in_ms;
}
