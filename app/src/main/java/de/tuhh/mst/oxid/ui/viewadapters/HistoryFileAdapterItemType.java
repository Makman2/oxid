package de.tuhh.mst.oxid.ui.viewadapters;


public enum HistoryFileAdapterItemType {
    VOLTAGE_MEASUREMENT, IMPEDANCE_MEASUREMENT
}
