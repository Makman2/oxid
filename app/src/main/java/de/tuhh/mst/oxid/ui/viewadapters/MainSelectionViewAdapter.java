package de.tuhh.mst.oxid.ui.viewadapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.tuhh.mst.oxid.R;


/**
 * A view adapter for displaying an item in the main selection.
 */
public class MainSelectionViewAdapter extends ArrayAdapter<MainSelectionItem> {
    /**
     * Instantiates a new {@link MainSelectionViewAdapter}.
     *
     * @param context The context.
     */
    public MainSelectionViewAdapter(Context context) {
        super(context, R.layout.main_selection_item, R.id.main_selection_item_text);
    }

    /**
     * Instantiates a new {@link MainSelectionViewAdapter} with an initial dataset.
     *
     * @param context The context.
     * @param items   The initial dataset.
     */
    public MainSelectionViewAdapter(Context context, List<MainSelectionItem> items) {
        super(context, R.layout.main_selection_item, R.id.main_selection_item_text, items);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;
        MainSelectionItem item = getItem(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.main_selection_item, null);
        }
        else {
            view = convertView;
        }

        if (item != null) {
            ImageView image_view = (ImageView)view.findViewById(R.id.main_selection_item_image);
            TextView text_view = (TextView)view.findViewById(R.id.main_selection_item_text);

            image_view.setImageResource(item.getImage());
            text_view.setText(item.getText());
        }

        return view;
    }
}
