package de.tuhh.mst.oxid.ui;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.math3.complex.Complex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.ImpedanceDataTable;
import de.tuhh.mst.oxid.data.storage.ImpedanceDataTableFileManager;
import de.tuhh.mst.oxid.math.estimators.Estimator;
import de.tuhh.mst.oxid.math.estimators.IdentityEstimator;
import de.tuhh.mst.oxid.math.estimators.MeanEstimator;
import de.tuhh.mst.oxid.math.estimators.RelativeCutoffEstimator;
import de.tuhh.mst.oxid.measurement.ImpedanceAnalyzer;
import de.tuhh.mst.oxid.measurement.ImpedanceResultAvailableListener;
import de.tuhh.mst.oxid.utilities.units;


/**
 * Presents sample audio impedance measurement.
 */
public class ImpedanceActivityFragment extends Fragment implements ImpedanceResultAvailableListener {

    public ImpedanceActivityFragment()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_impedance, container, false);

        _ui_absolute_textview = (TextView)view.findViewById(R.id.fragment_impedance_absolute);
        _ui_phase_textview = (TextView)view.findViewById(R.id.fragment_impedance_phase);
        _ui_imaginary_textview = (TextView)view.findViewById(R.id.fragment_impedance_imaginary);
        _ui_real_textview = (TextView)view.findViewById(R.id.fragment_impedance_real);
        _ui_calibrate_button = (Button)view.findViewById(R.id.fragment_impedance_calibrate_button);

        _ui_calibrate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _signal_processor.calibrate();
            }
        });

        _data_table = new ImpedanceDataTable(new ArrayList<Complex>());

        return view;
    }

    @Override
    public void onImpedanceResultAvailable(Complex impedance) {
        if (_last_display_time_delta_in_samples >= _mean_filter_size) {
            _data_table.data.add(impedance);

            _ui_absolute_textview.setText(units.formatUnitString(
                impedance.abs(), 2, "Ω"));
            _ui_phase_textview.setText(String.format(
                Locale.getDefault(), "%.2f°", Math.toDegrees(impedance.getArgument())));

            units.MetricPrefix prefix_real =
                units.convertToNearestMetricPrefix(impedance.getReal()).getValue();
            units.MetricPrefix prefix_imaginary =
                units.convertToNearestMetricPrefix(impedance.getImaginary()).getValue();
            units.MetricPrefix biggest_prefix =
                prefix_real.compareTo(prefix_imaginary) > 0 ? prefix_real : prefix_imaginary;

            _ui_real_textview.setText(units.formatUnitString(
                impedance.getReal(), 2, biggest_prefix, "Ω"));
            _ui_imaginary_textview.setText(units.formatUnitString(
                impedance.getImaginary(), 2, biggest_prefix, "Ω"));

            _last_display_time_delta_in_samples = 0;
        }
        else {
            _last_display_time_delta_in_samples++;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Resources resources = getResources();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        final int frequency = Integer.parseInt(preferences.getString(
            getString(R.string.preference_measuring_frequency_key),
            resources.getString(R.string.preference_measuring_frequency_default)));

        final double amplitude = Double.parseDouble(preferences.getString(
            getString(R.string.preference_measuring_amplitude_key),
            resources.getString(R.string.preference_measuring_amplitude_default)));

        final int input_sampling_rate = Integer.parseInt(preferences.getString(
            resources.getString(R.string.preference_input_sampling_rate_key),
            resources.getString(R.string.preference_input_sampling_rate_default)));

        final double reference_impedance = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_reference_impedance_key),
            resources.getString(R.string.preference_reference_impedance_default)));

        final double sample_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_sample_size_key),
            resources.getString(R.string.preference_measuring_sample_size_default)));

        final int sample_size = (int)(input_sampling_rate * sample_size_in_ms / 1000.0);

        final double frequency_uncertainty = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_frequency_uncertainty_key),
            resources.getString(R.string.preference_measuring_frequency_uncertainty_default)));

        final double mean_filter_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_key),
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_default)));

        _mean_filter_size = (int)(mean_filter_size_in_ms / sample_size_in_ms);

        Estimator estimator1;
        Estimator estimator2;
        if (_mean_filter_size == 0) {
            estimator1 = new IdentityEstimator();
            estimator2 = new IdentityEstimator();
        }
        else {
            estimator1 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
            estimator2 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
        }

        _data_table.sampling_rate = input_sampling_rate;
        _data_table.frequency = frequency;
        _data_table.amplitude = amplitude;
        _data_table.sample_size_in_ms = sample_size_in_ms;
        _data_table.frequency_uncertainty = frequency_uncertainty;
        _data_table.mean_filter_size_in_ms = mean_filter_size_in_ms;
        _data_table.reference_impedance = new Complex(reference_impedance);

        // FIXME Calculate real correction factor.
        final double correction_factor = 1.0 / 12.0;
        final double correction_offset = 0.0;

        _signal_processor = new ImpedanceAnalyzer(
            amplitude,
            frequency,
            input_sampling_rate,
            sample_size,
            frequency_uncertainty,
            new Complex(reference_impedance),
            correction_factor,
            correction_offset,
            estimator1,
            estimator2,
            this);

        _signal_processor.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        _signal_processor.release();

        // Save data to file.
        try {
            ImpedanceDataTableFileManager.store(getContext(), new Date(), _data_table);

            Toast.makeText(
                getContext(),
                "Measurement data stored in history browser", Toast.LENGTH_SHORT).show();
        }
        catch (IOException ex) {
            Toast.makeText(
                getContext(),
                "Storing measurement data failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private int _mean_filter_size;
    private int _last_display_time_delta_in_samples;

    private ImpedanceAnalyzer _signal_processor;

    private ImpedanceDataTable _data_table;

    private TextView _ui_absolute_textview;
    private TextView _ui_phase_textview;
    private TextView _ui_real_textview;
    private TextView _ui_imaginary_textview;
    private Button _ui_calibrate_button;
}
