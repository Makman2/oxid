package de.tuhh.mst.oxid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.apache.commons.math3.util.Pair;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.storage.ImpedanceDataTableFileManager;
import de.tuhh.mst.oxid.data.storage.VoltageDataTableFileManager;
import de.tuhh.mst.oxid.ui.viewadapters.HistoryFileAdapter;
import de.tuhh.mst.oxid.ui.viewadapters.HistoryFileAdapterItemType;


public class HistoryBrowserActivityFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_browser, container, false);

        ListView list = (ListView)view.findViewById(R.id.fragment_history_browser_data_list);

        // Set the empty view for the ListView.
        View empty_view = inflater.inflate(
            R.layout.fragment_history_browser_no_data, list, false);
        getActivity().addContentView(empty_view, new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        list.setEmptyView(empty_view);

        _list_adapter = new HistoryFileAdapter(getActivity(), getListItems());
        list.setAdapter(_list_adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter_view, View view, int position, long id) {
                @SuppressWarnings("unchecked")
                Pair<File, HistoryFileAdapterItemType> item =
                    (Pair<File, HistoryFileAdapterItemType>)
                    adapter_view.getItemAtPosition(position);

                Intent start_activity_intent =
                    new Intent(getActivity(), HistoryPreviewActivity.class);

                start_activity_intent.putExtra(
                    IntentExtra.history_preview_activity_filename,
                    item.getKey().getAbsolutePath());
                start_activity_intent.putExtra(
                    IntentExtra.history_preview_activity_filetype,
                    item.getValue().name());

                startActivity(start_activity_intent);
            }
        });

        return view;
    }

    public static List<Pair<File, HistoryFileAdapterItemType>> getListItems() {
        List<Pair<File, HistoryFileAdapterItemType>> files = new ArrayList<>();

        for (File file : VoltageDataTableFileManager.getFiles()) {
            files.add(new Pair<>(file, HistoryFileAdapterItemType.VOLTAGE_MEASUREMENT));
        }
        for (File file : ImpedanceDataTableFileManager.getFiles()) {
            files.add(new Pair<>(file, HistoryFileAdapterItemType.IMPEDANCE_MEASUREMENT));
        }

        class FileComparator implements Comparator<Pair<File, HistoryFileAdapterItemType>> {
            @Override
            public int compare(Pair<File, HistoryFileAdapterItemType> f1, Pair<File, HistoryFileAdapterItemType> f2) {
                String f1name = f1.getKey().getName();
                String f2name = f2.getKey().getName();
                // Get the names without .csv extension.
                String f1date = f1name.substring(0, f1name.lastIndexOf("."));
                String f2date = f2name.substring(0, f2name.lastIndexOf("."));

                try {
                    DateFormat parser = new SimpleDateFormat(
                        "EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
                    Date d1 = parser.parse(f1date);
                    Date d2 = parser.parse(f2date);

                    // So most recent entries appear on top of the history browser list, we need to
                    // inverse sort order.
                    return d2.compareTo(d1);
                }
                catch (ParseException ex) {
                    // In case of error, don't sort files.
                    Log.w("oxid", "Failed to compare history files for sorting list: '" +
                          f1name + "' and '" + f2name + "'. Not sorting those.");

                    return 0;
                }
            }
        }

        // Sort files by date, most recent ones displayed at top.
        Collections.sort(files, new FileComparator());
        
        return files;
    }

    /**
     * Refreshes the file display list.
     */
    public void updateList() {
        _list_adapter.clear();
        _list_adapter.addAll(getListItems());
        _list_adapter.notifyDataSetChanged();
    }

    private HistoryFileAdapter _list_adapter;
}
