package de.tuhh.mst.oxid.hardware;


/**
 * The listener interface for {@link AudioOutputGenerator} that generates buffer chunks containing
 * the signal to output as audio.
 */
public interface OnSignalQueriedListener {
    /**
     * Triggered when {@link AudioOutputGenerator} requests a buffer which contains the signal to
     * output as audio.
     *
     * @return The signal buffer. The values are mapped to a 16-bit PCM at hardware.
     */
    short[] get_signal();
}
