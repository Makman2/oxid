package de.tuhh.mst.oxid.math.estimators;

import java.nio.DoubleBuffer;


/**
 * An {@link Estimator} that uses the mean over a certain number of most-recent samples.
 *
 * This estimator solves the least-square-problem for constant model functions. I.e. it estimates a
 * constant trend by minimizing the squared-error to the samples.
 */
public class MeanEstimator extends Estimator {
    /**
     * Initializes a new {@link MeanEstimator} with a sample-count of 10.
     */
    public MeanEstimator() {
        this(10);
    }

    /**
     * Initializes a new {@link MeanEstimator}.
     *
     * @param samples The number of samples to estimate over.
     */
    public MeanEstimator(int samples) {
        _buffer = DoubleBuffer.allocate(samples);
        _estimation_sample_count = 0;
        _sum_cache = 0.0;
    }

    /**
     * Update estimation with a new value.
     *
     * Putting a new value into this {@link MeanEstimator} causes the oldest value from the last
     * sample-count-number of values to be not considered any more.
     *
     * Only values put-in by this function are significant for the estimation. I.e. on construction
     * or after calling {@link #reset()} the mean is taken only from existing samples and the
     * effective sample count increases until the value from {@link #getSampleCount()} is reached.
     *
     * @param value A new value to consider for estimation.
     */
    @Override
    public void put(double value) {
        if (_estimation_sample_count < _buffer.capacity()) {
            _estimation_sample_count++;
        }
        else {
            // Read current buffer position without advancing it.
            _sum_cache -= _buffer.get(_buffer.position());
        }

        _buffer.put(value);

        _sum_cache += value;

        if (!_buffer.hasRemaining()) {
            _buffer.position(0);
        }
    }

    /**
     * Performs the estimation.
     *
     * Calling this function leads to undefined behaviour when no values were previously inputted
     * via {@link #put(double)}.
     *
     * @return The estimation value.
     */
    @Override
    public double estimate() {
        return _sum_cache / (double)_estimation_sample_count;
    }

    @Override
    public void reset() {
        _buffer.position(0);
        _estimation_sample_count = 0;
        _sum_cache = 0.0;
    }

    /**
     * @return How many most-recent samples are considered for estimation currently.
     */
    protected int getCurrentSampleCount() {
        return _estimation_sample_count;
    }

    /**
     * @return How many most-recent samples are considered for estimation maximum. This is the value
     *         {@code samples} provided at construction.
     */
    public int getSampleCount() {
        return _buffer.capacity();
    }

    private DoubleBuffer _buffer;
    private int _estimation_sample_count;
    private double _sum_cache;
}
