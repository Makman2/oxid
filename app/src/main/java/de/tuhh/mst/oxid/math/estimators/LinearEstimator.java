package de.tuhh.mst.oxid.math.estimators;

import java.nio.DoubleBuffer;


/**
 * An {@link Estimator} that uses the linear trend over a certain number of most-recent samples.
 *
 * This estimator solves the least-square-problem for linear-affine model functions. I.e. it
 * estimates a linear-affine trend by minimizing the squared-error to the samples.
 */
public class LinearEstimator extends Estimator {
    /**
     * Initializes a new {@link LinearEstimator} with a sample-count of 10.
     */
    public LinearEstimator() {
        this(10);
    }

    /**
     * Initializes a new {@link LinearEstimator}.
     *
     * @param samples The number of samples to estimate over.
     */
    public LinearEstimator(int samples) {
        _buffer = DoubleBuffer.allocate(samples);
        _estimation_sample_count = 0;
        _sum_cache_xi = 0.0;
        _sum_cache_xi2 = 0.0;
        _sum_cache_yi = 0.0;
    }

    /**
     * Update estimation with a new value.
     *
     * Putting a new value into this {@link MeanEstimator} causes the oldest value from the last
     * sample-count-number of values to be not considered any more.
     *
     * Only values put-in by this function are significant for the estimation. I.e. on construction
     * or after calling {@link #reset()} the estimation only considers existing samples and the
     * effective sample count increases until the value from {@link #getSampleCount()} is reached.
     *
     * @param value A new value to consider for estimation.
     */
    @Override
    public void put(double value) {
        if (_estimation_sample_count < _buffer.capacity()) {
            _sum_cache_xi += _estimation_sample_count;
            _sum_cache_xi2 += _estimation_sample_count * _estimation_sample_count;
            _estimation_sample_count++;
        }
        else {
            // Read current buffer position without advancing it.
            _sum_cache_yi -= _buffer.get(_buffer.position());
        }

        _buffer.put(value);

        _sum_cache_yi += value;

        if (!_buffer.hasRemaining()) {
            _buffer.position(0);
        }
    }

    /**
     * Performs the estimation.
     *
     * Calling this function leads to undefined behaviour when not providing at least 2 values via
     * {@link #put(double)}.
     *
     * @return The estimation value.
     */
    @Override
    public double estimate() {
        double denominator = (double)_estimation_sample_count * _sum_cache_xi2 -
                             _sum_cache_xi * _sum_cache_xi;

        double sum_x1y1 = 0.0;
        double[] buffer_view = _buffer.array();
        // The order of input values is important, so we have to divide calculation into two loops
        // because of our buffer structure, that continuously overrides old values in the backing
        // array.
        int i = 0;
        for (int k = _buffer.position(); k < _estimation_sample_count; k++) {
            sum_x1y1 += i * buffer_view[k];
            i++;
        }
        for (int k = 0; k < _buffer.position(); k++) {
            sum_x1y1 += i * buffer_view[k];
            i++;
        }

        double b0 = (_sum_cache_xi2 * _sum_cache_yi - _sum_cache_xi * sum_x1y1);
        double b1 = ((double)_estimation_sample_count * sum_x1y1 - _sum_cache_xi * _sum_cache_yi);

        return (b1 * _estimation_sample_count + b0) / denominator;
    }

    @Override
    public void reset() {
        _buffer.position(0);
        _estimation_sample_count = 0;
        _sum_cache_xi = 0.0;
        _sum_cache_xi2 = 0.0;
        _sum_cache_yi = 0.0;
    }

    /**
     * @return How many most-recent samples are considered for estimation currently.
     */
    protected int getCurrentSampleCount() {
        return _estimation_sample_count;
    }

    /**
     * @return How many most-recent samples are considered for estimation maximum. This is the value
     *         {@code samples} provided at construction.
     */
    public int getSampleCount() {
        return _buffer.capacity();
    }

    private DoubleBuffer _buffer;
    private int _estimation_sample_count;
    private double _sum_cache_xi;
    private double _sum_cache_xi2;
    private double _sum_cache_yi;
}
