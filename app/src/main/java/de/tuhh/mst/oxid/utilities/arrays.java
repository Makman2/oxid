package de.tuhh.mst.oxid.utilities;

import java.nio.DoubleBuffer;


/**
 * Module that contains various array-related helper functions (e.g. type conversions).
 */
public final class arrays {
    private arrays() {}

    /**
     * Converts a {@code short} array to a {@code double} array.
     *
     * @param arr The array to convert.
     * @return    The converted array.
     */
    public static double[] toDouble(short[] arr) {
        DoubleBuffer data = DoubleBuffer.allocate(arr.length);
        for (short y : arr) {
            data.put(y);
        }
        return data.array();
    }
}
