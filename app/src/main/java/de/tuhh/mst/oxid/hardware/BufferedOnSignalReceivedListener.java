package de.tuhh.mst.oxid.hardware;

import java.nio.ShortBuffer;


/**
 * Base class for signal listeners for {@link AudioRecorderProcessor} that buffers the incoming
 * signal.
 */
public abstract class BufferedOnSignalReceivedListener implements OnSignalReceivedListener {
    /**
     * Creates a new {@link BufferedOnSignalReceivedListener}.
     *
     * @param buffer_size The size of the signal buffer.
     */
    public BufferedOnSignalReceivedListener(int buffer_size) {
        _signal_buffer = ShortBuffer.allocate(buffer_size);
    }

    @Override
    public final void process_signal(short[] signal) {
        int written = 0;

        while (written < signal.length) {
            int write_count = Math.min(_signal_buffer.capacity() - _signal_buffer.position(),
                                       signal.length - written);

            _signal_buffer.put(signal, written, write_count);

            if (!_signal_buffer.hasRemaining()) {
                process_buffered_signal(_signal_buffer.array().clone());

                _signal_buffer.clear();
            }

            written += write_count;
        }
    }

    /**
     * Processes the buffered signal.
     *
     * Override this method to process your signal.
     *
     * @param signal The signal received from {@link AudioRecorderProcessor}.
     */
    public abstract void process_buffered_signal(short[] signal);

    private ShortBuffer _signal_buffer;
}
