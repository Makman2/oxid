package de.tuhh.mst.oxid.ui.viewadapters;


/**
 * Contains information about a single item that can be displayed in the main selection menu.
 *
 * The layout consists of a left-aligned {@link android.widget.ImageView} and right beneath is a
 * {@link android.widget.TextView}.
 */
public class MainSelectionItem {
    /**
     * Instantiates a new item.
     *
     * @param image The resource ID of the image to display.
     * @param text  The text to display.
     */
    public MainSelectionItem(int image, CharSequence text) {
        _image = image;
        _text = text;
    }

    /**
     * Returns the resource ID of the item's image.
     *
     * @return The resource ID of an image.
     */
    public int getImage() {
        return _image;
    }

    /**
     * Returns the text to display on the item.
     *
     * @return The {@link CharSequence} to display.
     */
    public CharSequence getText() {
        return _text;
    }

    private int _image;
    private CharSequence _text;
}
