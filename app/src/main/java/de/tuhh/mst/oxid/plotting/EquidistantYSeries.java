package de.tuhh.mst.oxid.plotting;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class EquidistantYSeries extends ParcelableXYSeries {
    public EquidistantYSeries(List<? extends Number> y_values, String title) {
        this(y_values, 0.0, 1.0, title);
    }

    public EquidistantYSeries(List<? extends Number> y_values, double domain_step, String title) {
        this(y_values, 0.0, domain_step, title);
    }

    public EquidistantYSeries(List<? extends Number> y_values, double domain_offset, double domain_step, String title) {
        _y_values = y_values;
        _title = title;
        _domain_offset = domain_offset;
        _domain_step = domain_step;
    }

    @Override
    public int size() {
        return _y_values.size();
    }

    @Override
    public String getTitle() {
        return _title;
    }

    @Override
    public Number getX(int index) {
        return _domain_offset + _domain_step * index;
    }

    @Override
    public Number getY(int index) {
        return _y_values.get(index);
    }

    public List<? extends Number> getYValues() {
        return _y_values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_title);
        dest.writeDouble(_domain_offset);
        dest.writeDouble(_domain_step);
        dest.writeList(_y_values);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public EquidistantYSeries createFromParcel(Parcel in) {
            String title = in.readString();
            double domain_offset = in.readDouble();
            double domain_step = in.readDouble();
            @SuppressWarnings("unchecked")
            ArrayList<? extends Number> y_values =
                in.readArrayList(ClassLoader.getSystemClassLoader());

            return new EquidistantYSeries(y_values, domain_offset, domain_step, title);
        }

        public EquidistantYSeries[] newArray(int size) {
            return new EquidistantYSeries[size];
        }
    };

    private double _domain_offset;
    private double _domain_step;
    private String _title;
    private List<? extends Number> _y_values;
}
