package de.tuhh.mst.oxid.ui;


/**
 * Contains IDs for Intent values passed as parameters, in bundles etc.
 */
public final class IntentExtra
{
    private IntentExtra()
    {}

    /**
     * The parameter that is passed to {@link HistoryPreviewActivity} that identifies the file to
     * load for preview.
     */
    public static final String history_preview_activity_filename =
        "history_preview_activity_filename";

    /**
     * The parameter that is passed to {@link HistoryPreviewActivity} that tells whether the file
     * is a voltage measurement file or an impedance measurement file.
     */
    public static final String history_preview_activity_filetype =
        "history_preview_activity_filetype";
}
