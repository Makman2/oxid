package de.tuhh.mst.oxid.math.estimators;


/**
 * The {@link RelativeCutoffEstimator} can be applied on any other estimator. It resets the given
 * estimator as soon as the last value does differ too much relatively to the most recent one.
 */
public class RelativeCutoffEstimator extends CutoffEstimator {
    /**
     * Initializes a new {@link CutoffEstimator} with a relative deviation of 10%.
     *
     * @param base_estimator The base estimator.
     */
    public RelativeCutoffEstimator(Estimator base_estimator) {
        this(base_estimator, 0.1);
    }

    /**
     * Initializes a new {@link CutoffEstimator}.
     *
     * @param base_estimator     The base estimator.
     * @param relative_deviation The relative deviation that controls when the base estimator is
     *                           reset on new input.
     */
    public RelativeCutoffEstimator(Estimator base_estimator, double relative_deviation) {
        super(base_estimator);

        _relative_deviation = relative_deviation;
        _last_value = 0.0;
    }

    @Override
    protected boolean needs_reset(double value) {
        boolean result = Math.abs(value - _last_value) >
                         Math.abs(_last_value) * _relative_deviation;
        _last_value = value;
        return result;
    }

    @Override
    public void put(double value) {
        super.put(value);
        _last_value = value;
    }

    /**
     * @return The relative deviation that controls the reset of the base-estimator.
     */
    public double getRelativeDeviation() {
        return _relative_deviation;
    }

    private double _relative_deviation;
    private double _last_value;
}
