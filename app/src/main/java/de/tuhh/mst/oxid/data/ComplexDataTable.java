package de.tuhh.mst.oxid.data;

import org.apache.commons.math3.complex.Complex;

import java.util.List;


public class ComplexDataTable {
    public ComplexDataTable(List<Complex> data) {
        this.data = data;
    }

    public List<Complex> data;
}
