package de.tuhh.mst.oxid.utilities;


/**
 * Module that contains various threading related helper functions.
 */
public final class threading {
    private threading() {}

    /**
     * Joins a thread.
     *
     * As {@link Thread} requires to wrap {@link Thread#join()} inside a try-except statement,
     * because the join could be interrupted, this function takes over the
     * {@link InterruptedException} and retries the join forever until it succeeds.
     */
    public static void joinThread(Thread thread) {
        while (true) {
            try {
                thread.join();
                break;
            }
            catch (InterruptedException e) {
                continue;
            }
        }
    }
}
