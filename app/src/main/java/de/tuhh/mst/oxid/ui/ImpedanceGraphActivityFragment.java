package de.tuhh.mst.oxid.ui;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidplot.util.Redrawer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.XYPlot;

import org.apache.commons.math3.complex.Complex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.ImpedanceDataTable;
import de.tuhh.mst.oxid.data.storage.ImpedanceDataTableFileManager;
import de.tuhh.mst.oxid.math.estimators.Estimator;
import de.tuhh.mst.oxid.math.estimators.IdentityEstimator;
import de.tuhh.mst.oxid.math.estimators.MeanEstimator;
import de.tuhh.mst.oxid.math.estimators.RelativeCutoffEstimator;
import de.tuhh.mst.oxid.measurement.ImpedanceAnalyzer;
import de.tuhh.mst.oxid.measurement.ImpedanceResultAvailableListener;
import de.tuhh.mst.oxid.plotting.RunningSeries;
import de.tuhh.mst.oxid.plotting.RunningSeriesTracker;


public class ImpedanceGraphActivityFragment extends Fragment implements ImpedanceResultAvailableListener {
    private final int DISPLAY_RANGE = 100;

    public ImpedanceGraphActivityFragment()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_impedance_graph, container, false);

        _plot = (XYPlot)view.findViewById(R.id.fragment_impedance_graph_plot);
        _plot.getLegend().setVisible(false);
        _plot.setRangeBoundaries(0, 100000, BoundaryMode.FIXED);
        _plot.setDomainBoundaries(0, DISPLAY_RANGE, BoundaryMode.FIXED);

        _redrawer = new Redrawer(_plot, 10, false);

        _data_table = new ImpedanceDataTable(new ArrayList<Complex>());

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LineAndPointFormatter formatter = new LineAndPointFormatter();
        formatter.setVertexPaint(null);
        formatter.getLinePaint().setColor(0xFFAA00AA);
        formatter.getFillPaint().setColor(0x00000000);

        _series = new RunningSeries("impedance");

        _plot.addSeries(_series, formatter);

        _plot_tracker = new RunningSeriesTracker(_plot, _series, DISPLAY_RANGE);

        _redrawer.start();
    }

    @Override
    public void onImpedanceResultAvailable(Complex impedance) {
        if (_last_display_time_delta_in_samples >= _mean_filter_size) {
            _data_table.data.add(impedance);

            _series.add(impedance.abs());

            _plot_tracker.updateBoundaries();

            _last_display_time_delta_in_samples = 0;
        }
        else {
            _last_display_time_delta_in_samples++;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Resources resources = getResources();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        final int frequency = Integer.parseInt(preferences.getString(
            getString(R.string.preference_measuring_frequency_key),
            resources.getString(R.string.preference_measuring_frequency_default)));

        final double amplitude = Double.parseDouble(preferences.getString(
            getString(R.string.preference_measuring_amplitude_key),
            resources.getString(R.string.preference_measuring_amplitude_default)));

        final int input_sampling_rate = Integer.parseInt(preferences.getString(
            resources.getString(R.string.preference_input_sampling_rate_key),
            resources.getString(R.string.preference_input_sampling_rate_default)));

        final double reference_impedance = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_reference_impedance_key),
            resources.getString(R.string.preference_reference_impedance_default)));

        final double sample_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_sample_size_key),
            resources.getString(R.string.preference_measuring_sample_size_default)));

        final int sample_size = (int)(input_sampling_rate * sample_size_in_ms / 1000.0);

        final double frequency_uncertainty = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_frequency_uncertainty_key),
            resources.getString(R.string.preference_measuring_frequency_uncertainty_default)));

        final double mean_filter_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_key),
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_default)));

        _mean_filter_size = (int)(mean_filter_size_in_ms / sample_size_in_ms);

        Estimator estimator1;
        Estimator estimator2;
        if (_mean_filter_size == 0) {
            estimator1 = new IdentityEstimator();
            estimator2 = new IdentityEstimator();
        }
        else {
            estimator1 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
            estimator2 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
        }

        _data_table.sampling_rate = input_sampling_rate;
        _data_table.frequency = frequency;
        _data_table.amplitude = amplitude;
        _data_table.sample_size_in_ms = sample_size_in_ms;
        _data_table.frequency_uncertainty = frequency_uncertainty;
        _data_table.mean_filter_size_in_ms = mean_filter_size_in_ms;
        _data_table.reference_impedance = new Complex(reference_impedance);

        // FIXME Calculate real correction factor.
        final double correction_factor = 1.0 / 12.0;
        final double correction_offset = 0.0;

        _signal_processor = new ImpedanceAnalyzer(
            amplitude,
            frequency,
            input_sampling_rate,
            sample_size,
            frequency_uncertainty,
            new Complex(reference_impedance),
            correction_factor,
            correction_offset,
            estimator1,
            estimator2,
            this);

        _signal_processor.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        _signal_processor.release();
        _redrawer.finish();

        // Save data to file.
        try {
            ImpedanceDataTableFileManager.store(getContext(), new Date(), _data_table);

            Toast.makeText(
                getContext(),
                "Measurement data stored in history browser", Toast.LENGTH_SHORT).show();
        }
        catch (IOException ex) {
            Toast.makeText(
                getContext(),
                "Storing measurement data failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private int _mean_filter_size;
    private int _last_display_time_delta_in_samples;

    private ImpedanceAnalyzer _signal_processor;

    private ImpedanceDataTable _data_table;

    private XYPlot _plot;
    private RunningSeriesTracker _plot_tracker;
    private Redrawer _redrawer;
    private RunningSeries _series;
}
