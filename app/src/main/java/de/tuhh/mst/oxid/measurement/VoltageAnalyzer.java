package de.tuhh.mst.oxid.measurement;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexUtils;

import java.nio.DoubleBuffer;

import de.tuhh.mst.oxid.hardware.AudioOutputGeneratorBase;
import de.tuhh.mst.oxid.hardware.AudioRecorderProcessor;
import de.tuhh.mst.oxid.hardware.BufferedOnSignalReceivedListener;
import de.tuhh.mst.oxid.hardware.SineAudioOutputGenerator;
import de.tuhh.mst.oxid.math.estimators.Estimator;
import de.tuhh.mst.oxid.math.estimators.IdentityEstimator;
import de.tuhh.mst.oxid.utilities.arrays;
import de.tuhh.mst.oxid.utilities.math;


public abstract class VoltageAnalyzer extends BufferedOnSignalReceivedListener {
    class ProcessSignalAsyncTask extends AsyncTask<short[], Void, Complex> {
        public ProcessSignalAsyncTask(double phase) {
            _phase = phase;
        }

        @Override
        protected Complex doInBackground(short[]... signals) {
            if (signals.length > 1) {
                Log.w(ProcessSignalAsyncTask.class.getName() + ".doInBackground()",
                      "Only 1 signal supported, but passed " + signals.length + ". Ignoring " +
                          "additional signals.");
            }

            return process_signal(signals[0]);
        }

        protected Complex process_signal(short[] signal) {
            Complex[] transformed = math.rfft(arrays.toDouble(signal));

            // We need to weight the coming integral/sum with the frequency difference from one
            // index to another.
            final double frequency_step =
                (double)(_recorder.sampling_rate / 2) / (transformed.length - 1);

            // We assume an uncertainty in frequency for a linear system around the input-frequency
            // and integrate the absolute value over this area (approximately using the rectangle
            // rule).
            final int index_uncertainty_one_sided = (int)Math.ceil(
                _frequency_uncertainty / (2.0 * frequency_step));

            // The index where the frequency peak is located.
            final int frequency_index = (int)((double)_frequency / frequency_step);

            // For performance reasons only get abs() and arg() of the part we process further.
            int lower_bound = Math.max(0,
                frequency_index - index_uncertainty_one_sided);
            int upper_bound = Math.min(transformed.length - 1,
                frequency_index + index_uncertainty_one_sided);

            DoubleBuffer transformed_abs = DoubleBuffer.allocate(upper_bound - lower_bound + 1);
            for (int i = lower_bound; i <= upper_bound; i++) {
                transformed_abs.put(transformed[i].abs());
            }

            double measured_voltage_abs =
                // Integrate.
                math.sum(transformed_abs.array()) * frequency_step;

            double measured_voltage_arg = transformed[frequency_index].getArgument() + _phase;

            _voltage_absolute_estimator.put(measured_voltage_abs);
            _voltage_phase_estimator.put(measured_voltage_arg);

            Complex voltage = ComplexUtils.polar2Complex(
                _voltage_absolute_estimator.estimate(),
                _voltage_phase_estimator.estimate());

            return voltage;
        }

        @Override
        protected void onPostExecute(Complex voltage) {
            onVoltageAvailable(voltage);
        }

        private double _phase;
    }

    public VoltageAnalyzer(double output_voltage,
                           int frequency,
                           int sampling_rate,
                           int sample_size,
                           double frequency_uncertainty,
                           Estimator voltage_absolute_estimator,
                           Estimator voltage_phase_estimator) {
        super(sample_size);

        _frequency = frequency;
        _frequency_uncertainty = frequency_uncertainty;

        _angular_velocity = 2 * Math.PI * frequency / sampling_rate;

        _voltage_absolute_estimator = voltage_absolute_estimator;
        _voltage_phase_estimator = voltage_phase_estimator;

        _recorder = new AudioRecorderProcessor(sampling_rate, this);
        _signal_generator = new SineAudioOutputGenerator(frequency, output_voltage, 0.0);
    }

    public VoltageAnalyzer(double output_voltage,
                           int frequency,
                           int sampling_rate,
                           int sample_size) {
        this(output_voltage,
             frequency,
             sampling_rate,
             sample_size,
             20.0,
             new IdentityEstimator(),
             new IdentityEstimator());
    }

    @Override
    public void process_buffered_signal(short[] signal) {
        ProcessSignalAsyncTask task = new ProcessSignalAsyncTask(_phase);

        _phase -= signal.length * _angular_velocity;

        task.execute(signal);
    }

    /**
     * Called when a voltage was recently calculated.
     *
     * Override this method to process the measured voltage value further.
     *
     * @param voltage The voltage value.
     */
    protected abstract void onVoltageAvailable(Complex voltage);

    public void start() {
        _signal_generator.start();
        _recorder.start();
    }

    public void stop() {
        _recorder.stop();
        _signal_generator.stop();
    }

    public void release() {
        stop();
        _signal_generator.release();
        _recorder.release();
    }

    private final int _frequency;
    private final double _frequency_uncertainty;
    // Angular velocity measured in radians per sample.
    private final double _angular_velocity;
    private double _phase;
    // Estimators
    private Estimator _voltage_absolute_estimator;
    private Estimator _voltage_phase_estimator;
    // Hardware interfaces.
    private AudioRecorderProcessor _recorder;
    private AudioOutputGeneratorBase _signal_generator;
}
