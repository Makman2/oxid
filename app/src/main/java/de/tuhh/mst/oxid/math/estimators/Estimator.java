package de.tuhh.mst.oxid.math.estimators;


/**
 * An estimator serves the purpose of estimating data using already existent data.
 */
public abstract class Estimator {
    /**
     * Inputs a new value that shall be considered for estimation.
     *
     * @param value A new value to consider for estimation.
     */
    public abstract void put(double value);

    /**
     * Performs the estimation.
     *
     * @return The estimation value.
     */
    public abstract double estimate();

    /**
     * Resets the current {@link Estimator} instance by throwing away all data in consideration
     * until now.
     */
    public abstract void reset();
}
