package de.tuhh.mst.oxid.hardware;


/**
 * The listener interface for {@link AudioRecorderProcessor} that receives buffer chunks containing
 * the signal recorded.
 */
public interface OnSignalReceivedListener {
    /**
     * Triggered when a new signal-buffer-chunk is ready.
     *
     * @param signal The signal-buffer-chunk recorded.
     */
    void process_signal(short[] signal);
}
