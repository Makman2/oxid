package de.tuhh.mst.oxid.ui;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.VoltageDataTable;
import de.tuhh.mst.oxid.data.storage.VoltageDataTableFileManager;
import de.tuhh.mst.oxid.math.estimators.Estimator;
import de.tuhh.mst.oxid.math.estimators.IdentityEstimator;
import de.tuhh.mst.oxid.math.estimators.MeanEstimator;
import de.tuhh.mst.oxid.math.estimators.RelativeCutoffEstimator;
import de.tuhh.mst.oxid.measurement.VoltageAnalyzer;
import de.tuhh.mst.oxid.utilities.units;


/**
 * Presents sample audio impedance measurement.
 */
public class VoltageActivityFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_voltage, container, false);

        _ui_absolute_textview = (TextView)view.findViewById(R.id.fragment_voltage_absolute);
        _ui_phase_textview = (TextView)view.findViewById(R.id.fragment_voltage_phase);
        _ui_imaginary_textview = (TextView)view.findViewById(R.id.fragment_voltage_imaginary);
        _ui_real_textview = (TextView)view.findViewById(R.id.fragment_voltage_real);
        _ui_calibrate_button = (Button)view.findViewById(R.id.fragment_voltage_calibrate_button);

        _phase_offset = 0.0;
        _last_voltage = Complex.ZERO;

        _ui_calibrate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _phase_offset = -_last_voltage.getArgument();
            }
        });

        _data_table = new VoltageDataTable(new ArrayList<Complex>());

        return view;
    }

    private class MyVoltageAnalyzer extends VoltageAnalyzer {
        public MyVoltageAnalyzer(double output_voltage,
                                 int frequency,
                                 int sampling_rate,
                                 int sample_size,
                                 double frequency_uncertainty,
                                 Estimator voltage_absolute_estimator,
                                 Estimator voltage_argument_estimator) {
            super(output_voltage,
                  frequency,
                  sampling_rate,
                  sample_size,
                  frequency_uncertainty,
                  voltage_absolute_estimator,
                  voltage_argument_estimator);
        }

        @Override
        protected void onVoltageAvailable(Complex voltage) {
            if (_last_display_time_delta_in_samples >= _mean_filter_size) {
                _last_voltage = voltage;
                voltage = voltage.multiply(ComplexUtils.polar2Complex(1.0, _phase_offset));
                // Apply correction factor to map to effective voltage.
                voltage = voltage.multiply(1.2460063897763578e-05);

                _data_table.data.add(voltage);

                _ui_absolute_textview.setText(units.formatUnitString(
                    voltage.abs(), 2, "V"));
                _ui_phase_textview.setText(String.format(
                    Locale.getDefault(), "%.2f°", Math.toDegrees(voltage.getArgument())));

                units.MetricPrefix prefix_real =
                    units.convertToNearestMetricPrefix(voltage.getReal()).getValue();
                units.MetricPrefix prefix_imaginary =
                    units.convertToNearestMetricPrefix(voltage.getImaginary()).getValue();
                units.MetricPrefix biggest_prefix =
                    prefix_real.compareTo(prefix_imaginary) > 0 ? prefix_real : prefix_imaginary;

                _ui_real_textview.setText(units.formatUnitString(
                    voltage.getReal(), 2, biggest_prefix, "V"));
                _ui_imaginary_textview.setText(units.formatUnitString(
                    voltage.getImaginary(), 2, biggest_prefix, "V"));

                _last_display_time_delta_in_samples = 0;
            }
            else {
                _last_display_time_delta_in_samples++;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Resources resources = getResources();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        final int frequency = Integer.parseInt(preferences.getString(
            getString(R.string.preference_measuring_frequency_key),
            resources.getString(R.string.preference_measuring_frequency_default)));

        final double amplitude = Double.parseDouble(preferences.getString(
            getString(R.string.preference_measuring_amplitude_key),
            resources.getString(R.string.preference_measuring_amplitude_default)));

        final int input_sampling_rate = Integer.parseInt(preferences.getString(
            resources.getString(R.string.preference_input_sampling_rate_key),
            resources.getString(R.string.preference_input_sampling_rate_default)));

        final double sample_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_sample_size_key),
            resources.getString(R.string.preference_measuring_sample_size_default)));

        final int sample_size = (int)(input_sampling_rate * sample_size_in_ms / 1000.0);

        final double frequency_uncertainty = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_frequency_uncertainty_key),
            resources.getString(R.string.preference_measuring_frequency_uncertainty_default)));

        final double mean_filter_size_in_ms = Double.parseDouble(preferences.getString(
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_key),
            resources.getString(R.string.preference_measuring_mean_filter_buffer_size_default)));

        _mean_filter_size = (int)(mean_filter_size_in_ms / sample_size_in_ms);
        // We want to trigger an immediate display of the first value, so we set the counter
        // controlling the display to mean-filter-size which results in displaying the result.
        _last_display_time_delta_in_samples = _mean_filter_size;

        Estimator estimator1;
        Estimator estimator2;
        if (_mean_filter_size == 0) {
            estimator1 = new IdentityEstimator();
            estimator2 = new IdentityEstimator();
        }
        else {
            estimator1 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
            estimator2 = new RelativeCutoffEstimator(new MeanEstimator(_mean_filter_size));
        }

        _data_table.sampling_rate = input_sampling_rate;
        _data_table.frequency = frequency;
        _data_table.amplitude = amplitude;
        _data_table.sample_size_in_ms = sample_size_in_ms;
        _data_table.frequency_uncertainty = frequency_uncertainty;
        _data_table.mean_filter_size_in_ms = mean_filter_size_in_ms;

        _signal_processor = new MyVoltageAnalyzer(
            amplitude,
            frequency,
            input_sampling_rate,
            sample_size,
            frequency_uncertainty,
            estimator1,
            estimator2);

        _signal_processor.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        _signal_processor.release();

        // Save data to file.
        try {
            VoltageDataTableFileManager.store(getContext(), new Date(), _data_table);

            Toast.makeText(
                getContext(),
                "Measurement data stored in history browser", Toast.LENGTH_SHORT).show();
        }
        catch (IOException ex) {
            Toast.makeText(
                getContext(),
                "Storing measurement data failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private double _phase_offset;
    private Complex _last_voltage;

    private int _mean_filter_size;
    private int _last_display_time_delta_in_samples;

    private VoltageAnalyzer _signal_processor;

    private VoltageDataTable _data_table;

    private TextView _ui_absolute_textview;
    private TextView _ui_phase_textview;
    private TextView _ui_real_textview;
    private TextView _ui_imaginary_textview;
    private Button _ui_calibrate_button;
}
