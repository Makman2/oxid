package de.tuhh.mst.oxid.ui;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.data.storage.ImpedanceDataTableFileManager;
import de.tuhh.mst.oxid.data.storage.VoltageDataTableFileManager;
import de.tuhh.mst.oxid.ui.dialogs.YesNoDialogFragment;


public class HistoryBrowserActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_browser);
        Toolbar toolbar = (Toolbar)findViewById(R.id.activity_history_browser_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_menu_history_wipe) {
            YesNoDialogFragment dialog = new YesNoDialogFragment();
            dialog.setMessage(R.string.HistoryBrowserActivity_wipe_question);
            dialog.setCancelable(true);
            dialog.setOnYesListener(
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Log.d("oxid", "Wiping all measurement files...");

                        VoltageDataTableFileManager.wipe(getApplicationContext());
                        ImpedanceDataTableFileManager.wipe(getApplicationContext());

                        // Update file list of contained fragment.
                        Fragment fragment = getSupportFragmentManager().findFragmentById(
                            R.id.activity_history_browser_fragment);
                        if (fragment instanceof HistoryBrowserActivityFragment)
                        {
                            HistoryBrowserActivityFragment converted_fragment =
                                (HistoryBrowserActivityFragment)fragment;
                            converted_fragment.updateList();
                        }
                    }
                });

            dialog.setOnNoListener(null);
            dialog.show(getFragmentManager(), null);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
