package de.tuhh.mst.oxid.ui;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import de.tuhh.mst.oxid.R;


public class FFTPreferenceActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_fft);
    }
}
