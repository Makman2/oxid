package de.tuhh.mst.oxid.measurement;

import org.apache.commons.math3.complex.Complex;


public interface ImpedanceResultAvailableListener {
    void onImpedanceResultAvailable(Complex impedance);
}
