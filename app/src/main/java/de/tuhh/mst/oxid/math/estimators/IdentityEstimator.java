package de.tuhh.mst.oxid.math.estimators;


/**
 * Estimator that considers the last input value as an estimation.
 *
 * On instantiation, the estimation is 0.
 */
public class IdentityEstimator extends Estimator {
    /**
     * Constructs a new {@link IdentityEstimator}.
     */
    public IdentityEstimator() {
        _last_value = 0.0;
    }

    @Override
    public void put(double value) {
        _last_value = value;
    }

    @Override
    public double estimate() {
        return _last_value;
    }

    @Override
    public void reset() {
        _last_value = 0.0;
    }

    private double _last_value;
}
