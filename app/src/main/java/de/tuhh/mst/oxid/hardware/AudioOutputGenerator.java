package de.tuhh.mst.oxid.hardware;


/**
 * Plays an arbitrary long audio signal.
 */
public class AudioOutputGenerator extends AudioOutputGeneratorBase {
    /**
     * Instantiates a new {@link AudioOutputGenerator} without a listener.
     *
     * You need to call {@link #setOnSignalQueriedListener(OnSignalQueriedListener)} until you can
     * start playing audio.
     *
     * @param sampling_rate The sampling frequency of the output signal.
     */
    public AudioOutputGenerator(int sampling_rate) {
        super(sampling_rate);
    }

    /**
     * Instantiates a new {@link AudioOutputGenerator}.
     *
     * @param sampling_rate The sampling frequency of the output signal.
     * @param listener      The {@link OnSignalQueriedListener} that is triggered when a chunk of
     *                      the signal to play is requested.
     */
    public AudioOutputGenerator(int sampling_rate, OnSignalQueriedListener listener) {
        super(sampling_rate, listener);
    }

    /**
     * Sets the listener that is queried for buffer chunks that shall be played as audio.
     *
     * You must set a listener before you start the recording via {@link #start()}.
     *
     * @param listener               The listener to set.
     * @throws IllegalStateException Thrown when currently playing.
     */
    public void setOnSignalQueriedListener(OnSignalQueriedListener listener) {
        super.setOnSignalQueriedListener(listener);
    }

    /**
     * Gets the listener that is queried for buffer chunks that shall be played as audio.
     *
     * If no listener is set, this function returns {@code null}.
     *
     * @return The listener currently set.
     */
    public OnSignalQueriedListener getOnSignalQueriedListener() {
        return super.getOnSignalQueriedListener();
    }
}
