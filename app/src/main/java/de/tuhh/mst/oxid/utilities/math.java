package de.tuhh.mst.oxid.utilities;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.apache.commons.math3.util.OpenIntToDoubleHashMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * Module that contains various math functions.
 */
public final class math {
    private math() {}

    /**
     * Calculates the base-2-logarithm.
     *
     * @param x The number to apply the logarithm to.
     * @return  The base-2-logarithm of {@code x}.
     */
    public static int log2(int x) {
        if (x <= 0) {
            throw new IllegalArgumentException();
        }
        return Integer.SIZE - 1 - Integer.numberOfLeadingZeros(x);
    }

    /**
     * Exponentiates the given {@code base} with {@code exp}.
     *
     * @param base The base.
     * @param exp  The exponent.
     * @return     {@code base ^ exp}.
     */
    public static int pow(int base, int exp) {
        int result = 1;
        for (int i = 1; i <= exp; i++) {
            result *= base;
        }
        return result;
    }

    /**
     * Performs a DFT (Discrete Fourier Transform) on the given {@code signal}.
     *
     * The returned DFT contains complex numbers and has "standard" order (with
     * {@code F = dft(signal)} and {@code n = signal.length}): {@code F[0]} contains the
     * zero-frequency term, {@code F[1]} until {@code F[n/2]} (exclusively) contains the
     * positive-frequency terms, {@code F[n/2+1]} until {@code F[n]} (exclusively) contains the
     * negative-frequency terms, in order of decreasing negative frequency. {@code F[n/2]}
     * represents both positive and negative Nyquist frequency.
     *
     * As the underlying algorithm needs signal lengths with power of 2, the input is internally
     * padded with zeros. That's why the returned result may have bigger size (rounded up to the
     * next power of 2 of the input signal length).
     *
     * No DFT normalization is applied.
     *
     * @param signal The time-signal to DFT-transform.
     * @return       The complex Discrete Fourier Transform of {@code signal}.
     */
    public static Complex[] dft(double[] signal) {
        // Pad with zeroes if necessary, as the algorithm only allows array lengths with power
        // of 2. Also use bitshifts instead of pow(2, x), as this is faster.
        int base2 = log2(signal.length);
        if (1 << base2 != signal.length) {
            int new_capacity = 1 << (base2 + 1);
            signal = Arrays.copyOf(signal, new_capacity);
        }

        return new FastFourierTransformer(DftNormalization.STANDARD).transform(
            signal, TransformType.FORWARD);
    }

    /**
     * Performs an FFT on real input.
     *
     * Because FFTs on real inputs are complex conjugate symmetric, the second half is discarded.
     * The resulting FFT is scaled to the amplitudes of {@code signal}.
     *
     * @param signal The signal to compute the real FFT for.
     * @return       The real FFT.
     */
    public static Complex[] rfft(double[] signal) {
        Complex[] transformed = dft(signal);

        double scale_factor1 = 1.0 / transformed.length;
        double scale_factor2 = scale_factor1 * 2;

        // Only the lower part is relevant for a real-input FFT.
        transformed = Arrays.copyOfRange(transformed, 0, transformed.length / 2 + 1);

        // Zero and Nyquist frequency shall not be scaled with the 2x factor.
        transformed[0] = transformed[0].multiply(scale_factor1);
        transformed[transformed.length - 1] = transformed[transformed.length - 1].multiply(scale_factor1);
        // But all other frequencies need a 2x.
        for (int i = 1; i < transformed.length - 1; i++) {
            transformed[i] = transformed[i].multiply(scale_factor2);
        }

        return transformed;
    }

    /**
     * Clamps a value to a given range.
     *
     * @param value The value to clamp.
     * @param min   The minimum value to clamp to if {@code value < min}.
     * @param max   The maximum value to clamp to if {@code value > max}.
     * @return      The clamped value.
     */
    public static int clamp(int value, int min, int max) {
        return Math.min(Math.max(value, min), max);
    }

    /**
     * Generates a smooth kernel that uses the Gaussian normal distribution function with a relative
     * stop threshold of {@code 0.002699796} (which is the area not covered inside the range
     * {@code +-3*sigma}, whereas {@code sigma} is the standard deviation).
     *
     * @param standard_deviation The standard deviation of the underlying Gaussian normal
     *                           distribution function.
     * @return                   The Gaussian smooth kernel.
     */
    public static OpenIntToDoubleHashMap compute_gauss_smooth_kernel(double standard_deviation) {
        return compute_gauss_smooth_kernel(standard_deviation, 0.002699796);
    }

    /**
     * Generates a smooth kernel that uses the Gaussian normal distribution function.
     *
     * @param standard_deviation The standard deviation of the underlying Gaussian normal
     *                           distribution function.
     * @param relative_threshold The threshold when to stop computing further values for the smooth
     *                           kernel (relative to the maximum of the Gaussian distribution
     *                           function, located at {@code x=0}).
     * @return                   The Gaussian smooth kernel.
     */
    public static OpenIntToDoubleHashMap compute_gauss_smooth_kernel(double standard_deviation,
                                                                     double relative_threshold) {
        OpenIntToDoubleHashMap kernel = new OpenIntToDoubleHashMap();

        NormalDistribution function = new NormalDistribution(0, standard_deviation);

        double y0 = function.density(0.0);
        kernel.put(0, y0);

        int x = 1;
        double y = function.density((double)x);
        while (y > relative_threshold * y0) {
            kernel.put(x, y);
            kernel.put(-x, y);

            x++;
            y = function.density((double)x);
        }

        return kernel;
    }

    /**
     * Generates a smooth kernel that does not smooth.
     *
     * @return The identity smooth kernel.
     */
    public static OpenIntToDoubleHashMap compute_identity_smooth_kernel() {
        return compute_average_smooth_kernel(0);
    }

    /**
     * Generates a smooth kernel that averages adjacent values.
     *
     * @param radius The number of neighbours to average. Using {@code 0} returns a kernel that does
     *               not modify the data applied on.
     * @return       The averaging smooth kernel.
     */
    public static OpenIntToDoubleHashMap compute_average_smooth_kernel(int radius) {
        // FIXME Make an error check for radius < 0.
        OpenIntToDoubleHashMap kernel = new OpenIntToDoubleHashMap();

        double average = 1.0 / (2 * radius + 1);

        kernel.put(0, average);
        for (int i = 1; i < radius + 1; i++) {
            kernel.put(i, average);
            kernel.put(-i, average);
        }

        return kernel;
    }

    /**
     * Applies a smooth kernel on a dataset.
     *
     * For kernel indices that are out of bounds, the next available border value of {@code data} is
     * taken.
     *
     * @param data          The dataset to apply smoothing on.
     * @param smooth_kernel The smooth kernel. This is a map which contains the relative indexes of
     *                      {@link data} as keys and the weightings as values.
     * @return              The smoothed data.
     */
    public static double[] smooth(double[] data, OpenIntToDoubleHashMap smooth_kernel) {
        // An integral type array is implicitly initialized to 0.
        double[] smoothed_data = new double[data.length];

        // FIXME The algorithm might be faster if iterating over the smooth kernel entries first,
        // FIXME  applying them on all data in a row, and then advance to the next smooth kernel
        // FIXME  entry.
        for (int i = 0; i < data.length; i++) {
            OpenIntToDoubleHashMap.Iterator smooth_kernel_iterator = smooth_kernel.iterator();
            try {
                while(true) {
                    smooth_kernel_iterator.advance();

                    smoothed_data[i] +=
                        data[clamp(smooth_kernel_iterator.key() + i, 0, data.length - 1)] *
                        smooth_kernel_iterator.value();
                }
            }
            catch (NoSuchElementException ex)
            {}
        }

        return smoothed_data;
    }

    /**
     * Detects local maximum peaks inside the given {@code data}.
     *
     * Rising saddle points are also detected, but only the leftmost edge.
     *
     * @param data The data to search peaks in.
     * @return     A list of indices pointing at the maximum peaks.
     */
    public static List<Integer> detect_maxima(double[] data) {
        List<Integer> peaks = new ArrayList<>();
        for (int i = 1; i < data.length - 1; i++) {
            if (data[i] > data[i - 1] && data[i] >= data[i + 1]) {
                peaks.add(i);
            }
        }
        return peaks;
    }

    /**
     * Detects the single global maximum of the given dataset.
     *
     * @param data The data to search the maximum value in.
     * @return     The index where the value was found. If {@code data} is empty, {@code -1} is
     *             returned. If there are more than one global maximum, the last one is picked.
     */
    public static int detect_global_maximum(double[] data) {
        double last_max = -Double.MAX_VALUE;
        int last_index = -1;

        for (int i = 0; i < data.length; i++) {
            double x = data[i];
            if (x >= last_max) {
                last_max = x;
                last_index = i;
            }
        }

        return last_index;
    }

    /**
     * Picks out the given elements denoted by their indices.
     *
     * @param indices The indices to pick (in iteration order).
     * @param data    The data to pick out from.
     * @return        A new array containing the picked out data.
     */
    public static double[] pick(List<Integer> indices, double[] data) {
        double[] picked = new double[indices.size()];
        int n = 0;
        for (int i : indices) {
            picked[n] = data[i];
            n++;
        }
        return picked;
    }

    /**
     * Sums up all values in the given array.
     *
     * @param series The series of values to sum up.
     * @return       The sum of all values in the array.
     */
    public static double sum(double[] series) {
        return sum(series, 0, series.length);
    }

    /**
     * Sums up values in the given array.
     *
     * @param series The series of values to sum up.
     * @param from   Index (inclusive) where to start summation.
     * @param to     Index (exclusive) where to stop summation.
     * @return       The sum of all values in {@code series[from:to]}. If {@code from > to},
     *               returns {@code 0}.
     */
    public static double sum(double[] series, int from, int to) {
        double sum = 0.0;
        for (int i = from; i < to; i++) {
            sum += series[i];
        }
        return sum;
    }

    /**
     * Sums up all complex values in the given array.
     *
     * @param series The series of complex values to sum up.
     * @return       The sum of all complex values in the array.
     */
    public static Complex sum(Complex[] series) {
        return sum(series, 0, series.length);
    }

    /**
     * Sums up complex values in the given array.
     *
     * @param series The series of complex values to sum up.
     * @param from   Index (inclusive) where to start summation.
     * @param to     Index (exclusive) where to stop summation.
     * @return       The sum of all complex values in {@code series[from:to]}. If {@code from > to},
     *               returns {@code 0}.
     */
    public static Complex sum(Complex[] series, int from, int to) {
        Complex sum = new Complex(0.0);
        for (int i = from; i < to; i++) {
            sum = sum.add(series[i]);
        }
        return sum;
    }

    /**
     * Calculates the mean of the elements inside the given array.
     *
     * @param series The series of floating point values to calculate mean for.
     * @return       The mean.
     */
    public static double mean(double[] series) {
        return sum(series) / series.length;
    }
}
