package de.tuhh.mst.oxid.hardware;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import java.util.IllegalFormatCodePointException;


/**
 * Plays a periodic audio signal.
 */
public class StaticAudioOutputGenerator {
    /**
     * Minimum buffer size in shorts when using {@link AudioTrack#MODE_STATIC}. Even though samples
     * below this size are possible to play, it's not possible to set loop points on that sample.
     */
    private static final int MIN_BUFFER_SIZE = 16;

    /**
     * Instantiates a new {@link StaticAudioOutputGenerator}.
     *
     * @param sampling_rate
     *     The sampling frequency of the output signal.
     * @param sample
     *     The actual sample to play. The output is resolved with a 16-bit PCM modulation.
     * @throws IllegalArgumentException
     *     Thrown when the sample buffer has a length of 0.
     */
    public StaticAudioOutputGenerator(int sampling_rate, short[] sample) {
        if (sample.length == 0) {
            throw new IllegalArgumentException("An empty audio sample isn't playable.");
        }

        this.sampling_rate = sampling_rate;
        _sample = sample;

        initializeAudioTrack();
    }

    /**
     * Performs an integer division and rounds the result upwards if there's a remainder.
     *
     * {@code
     * >>> roundedUpIntDivision(10, 5);
     * 2
     * >>> roundedUpIntDivision(11, 5);
     * 3
     * }
     *
     * @param divident The divident.
     * @param divisor  The divisor.
     * @return         The quotient, rounded upwards if there's a remainder.
     */
    private static int roundedUpIntDivision(int divident, int divisor) {
        return divident / divisor + (divident % divisor == 0 ? 0 : 1);
    }

    private void initializeAudioTrack() {
        short[] sample;

        // Multiply sample to fit the minimal buffer size if necessary.
        if (_sample.length < MIN_BUFFER_SIZE) {
            final int new_buffer_size =
                roundedUpIntDivision(MIN_BUFFER_SIZE, _sample.length) * _sample.length;

            sample = new short[new_buffer_size];

            for (int i = 0; i < new_buffer_size; i += _sample.length) {
                System.arraycopy(_sample, 0, sample, i, _sample.length);
            }
        }
        else
        {
            sample = _sample;
        }

        // 16-bit PCM coding is guaranteed to work on every device:
        // https://developer.android.com/reference/android/media/AudioFormat.html#ENCODING_PCM_16BIT
        _audio_track = new AudioTrack(AudioManager.STREAM_MUSIC,
                                      sampling_rate,
                                      AudioFormat.CHANNEL_OUT_MONO,
                                      AudioFormat.ENCODING_PCM_16BIT,
                                      sample.length * (Short.SIZE / Byte.SIZE),
                                      AudioTrack.MODE_STATIC);

        if (_audio_track.getState() != AudioTrack.STATE_NO_STATIC_DATA) {
            throw new RuntimeException("Underlying AudioTrack wasn't successfully initialized.");
        }

        int written = _audio_track.write(sample, 0, sample.length);
        if (written != sample.length) {
            throw new RuntimeException("Couldn't write complete sample (" + written + "/" +
                                       sample.length + ") to the underlying AudioTrack.");
        }

        int status_code = _audio_track.setLoopPoints(0, sample.length, -1);
        if (status_code != AudioTrack.SUCCESS) {
            throw new RuntimeException("Couldn't set infinite loop over the given sample. Error " +
                                       "code " + status_code + ".");
        }
    }

    /**
     * Plays the static signal buffer.
     */
    public void start() {
        _audio_track.play();
    }

    /**
     * Stops the audio output.
     *
     * This resets also the current playback position to the first frame in the sample buffer.
     *
     * Invoking {@link #start()} is possible after calling {@link #stop()}.
     */
    public void stop() {
        _audio_track.stop();
        _audio_track.reloadStaticData();
    }

    /**
     * Releases all resources occupied from the underlying {@link AudioTrack}.
     */
    public void release() {
        if (_audio_track != null) {
            _audio_track.release();
        }
    }

    /**
     * The sampling frequency of the output signal.
     */
    public final int sampling_rate;

    private final short[] _sample;
    private AudioTrack _audio_track;
}
