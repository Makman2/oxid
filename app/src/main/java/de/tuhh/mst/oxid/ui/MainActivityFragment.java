package de.tuhh.mst.oxid.ui;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import de.tuhh.mst.oxid.R;
import de.tuhh.mst.oxid.ui.viewadapters.MainSelectionItem;
import de.tuhh.mst.oxid.ui.viewadapters.MainSelectionViewAdapter;


/**
 * Contains the main selection view.
 */
public class MainActivityFragment extends Fragment implements AdapterView.OnItemClickListener {

    public MainActivityFragment()
    {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ListView list_view = (ListView)view.findViewById(R.id.fragment_main_listview);

        // Populate ListView with items.
        ArrayList<MainSelectionItem> items = new ArrayList<>();
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_FFT)));
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_voltage_measurement)));
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_voltage_graph_measurement)));
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_impedance_measurement)));
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_impedance_graph_measurement)));
        items.add(new MainSelectionItem(R.drawable.cube,
                                        getString(R.string.main_selection_history_browser)));

        list_view.setAdapter(new MainSelectionViewAdapter(view.getContext(), items));

        // Set listener.
        list_view.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MainSelectionItem item = (MainSelectionItem)adapterView.getItemAtPosition(position);

        Class next_fragment;

        CharSequence text = item.getText();
        if (text == getString(R.string.main_selection_FFT)) {
            next_fragment = FFTActivity.class;
        }
        else if (text == getString(R.string.main_selection_voltage_measurement)) {
            next_fragment = VoltageActivity.class;
        }
        else if (text == getString(R.string.main_selection_voltage_graph_measurement)) {
            next_fragment = VoltageGraphActivity.class;
        }
        else if (text == getString(R.string.main_selection_impedance_measurement)) {
            next_fragment = ImpedanceActivity.class;
        }
        else if (text == getString(R.string.main_selection_impedance_graph_measurement)) {
            next_fragment = ImpedanceGraphActivity.class;
        }
        else if (text == getString(R.string.main_selection_history_browser)) {
            next_fragment = HistoryBrowserActivity.class;
        }
        else {
            throw new AssertionError("Unexpected selection menu item clicked: " + text);
        }

        startActivity(new Intent(this.getContext(), next_fragment));
    }
}
