package de.tuhh.mst.oxid.data.storage;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.util.Log;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.complex.Complex;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.tuhh.mst.oxid.data.ImpedanceDataTable;


/**
 * Manages {@link ImpedanceDataTable} files.
 */
public final class ImpedanceDataTableFileManager {
    private ImpedanceDataTableFileManager()
    {}

    static {
        _filemanager = new FileManager("impedance-measurements");
    }

    /**
     * Save an {@link ImpedanceDataTable} to external storage.
     *
     * @param context      The application context.
     * @param timestamp    The timestamp of the {@link ImpedanceDataTable}. It will be used to
     *                     create the filename from.
     * @param data         The {@link ImpedanceDataTable} containing the data to store.
     * @throws IOException Thrown when IO errors occur.
     */
    public static void store(Context context,
                             Date timestamp,
                             ImpedanceDataTable data) throws IOException {
        store(context, getFilenameFromTimestamp(timestamp), data);
    }

    /**
     * Save an {@link ImpedanceDataTable} to external storage.
     *
     * @param context      The application context.
     * @param filename     The filename.
     * @param data         The {@link ImpedanceDataTable} containing the data to store.
     * @throws IOException Thrown when IO errors occur.
     */
    public static void store(Context context,
                             String filename,
                             ImpedanceDataTable data) throws IOException {
        final File file = _filemanager.getFile(filename);
        CSVPrinter printer = null;
        try {
            printer = CSVFormat.EXCEL.print(new FileWriter(file));

            // Print settings record.
            if (data.sampling_rate != null) {
                printer.print("sampling-rate=" + data.sampling_rate + "Hz");
            }
            if (data.frequency != null) {
                printer.print("frequency=" + data.frequency + "Hz");
            }
            if (data.amplitude != null) {
                printer.print("amplitude=" + data.amplitude);
            }
            if (data.sample_size_in_ms != null) {
                printer.print("sample-size=" + data.sample_size_in_ms + "ms");
            }
            if (data.frequency_uncertainty != null) {
                printer.print("frequency-uncertainty=" + data.frequency_uncertainty + "Hz");
            }
            if (data.mean_filter_size_in_ms != null) {
                printer.print("mean-filter-size=" + data.mean_filter_size_in_ms + "ms");
            }
            if (data.reference_impedance != null) {
                printer.print("reference-impedance=" + toString(data.reference_impedance));
            }
            printer.println();

            // Print header.
            printer.printRecord("real", "imaginary");

            // Print data.
            for (Complex value : data.data) {
                printer.printRecord(value.getReal(), value.getImaginary());
            }
        }
        finally {
            if (printer != null) {
                printer.close();
                MediaScannerConnection.scanFile(
                    context, new String[]{file.getAbsolutePath()}, null, null);
            }
        }
    }

    /**
     * Loads an {@link ImpedanceDataTable} from external storage.
     *
     * @param timestamp    The timestamp that identifies the {@link ImpedanceDataTable} to load.
     * @return             A {@link ImpedanceDataTable}.
     * @throws IOException Thrown when IO errors occur.
     */
    public static ImpedanceDataTable load(Date timestamp) throws IOException {
        return load(getFilenameFromTimestamp(timestamp));
    }

    /**
     * Loads an {@link ImpedanceDataTable} from external storage.
     *
     * @param filename     The filename to load from.
     * @return             A {@link ImpedanceDataTable}.
     * @throws IOException Thrown when IO errors occur.
     */
    public static ImpedanceDataTable load(String filename) throws IOException {
        File file = _filemanager.getFile(filename);

        CSVParser reader = null;

        List<Complex> data = new ArrayList<>();
        ImpedanceDataTable data_table = new ImpedanceDataTable(data);
        try {
            reader = CSVFormat.EXCEL.parse(new FileReader(file));

            Iterator<CSVRecord> iterator = reader.iterator();

            // Parse settings record.
            CSVRecord settings_record = iterator.next();

            for (String elem : settings_record) {
                String separator = "=";
                int assignment_operator_position = elem.indexOf(separator);
                if (assignment_operator_position != -1) {
                    String setting = elem.substring(0, assignment_operator_position);
                    String value = elem.substring(
                        assignment_operator_position + separator.length());

                    switch (setting) {
                        case "sampling-rate":
                            data_table.sampling_rate = (int)parseUnitString(value, "Hz");
                            break;
                        case "frequency":
                            data_table.frequency = (int)parseUnitString(value, "Hz");
                            break;
                        case "amplitude":
                            data_table.amplitude = Double.valueOf(value);
                            break;
                        case "sample-size":
                            data_table.sample_size_in_ms = parseUnitString(value, "ms");
                            break;
                        case "frequency-uncertainty":
                            data_table.frequency_uncertainty = parseUnitString(value, "Hz");
                            break;
                        case "mean-filter-size":
                            data_table.mean_filter_size_in_ms = parseUnitString(value, "ms");
                            break;
                        case "reference-impedance":
                            String float_regex = "(?:-|\\+)?\\d+(?:\\.\\d+)?";
                            Pattern regex = Pattern.compile(
                                "(" + float_regex + ")(" + float_regex + ")j");
                            Matcher matcher = regex.matcher(value);

                            if (matcher.matches()) {
                                data_table.reference_impedance = new Complex(
                                    Double.valueOf(matcher.group(1)),
                                    Double.valueOf(matcher.group(2)));
                            }
                            else {
                                throw new NumberFormatException(
                                    "Invalid complex value for setting 'reference-impedance': " +
                                    value);
                            }


                        // Ignore other settings.
                    }
                }
            }

            // Ignore header record.
            iterator.next();

            // Parse data.
            while (iterator.hasNext()) {
                CSVRecord record = iterator.next();

                data.add(new Complex(
                    Double.valueOf(record.get(0)),
                    Double.valueOf(record.get(1))));
            }
        }
        finally {
            if (reader != null) {
                reader.close();
            }
        }

        return data_table;
    }

    /**
     * Returns all files stored by this manager.
     *
     * @return An array of {@link File} descriptors.
     */
    public static File[] getFiles() {
        return _filemanager.getDirectory().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.endsWith(".csv");
            }
        });
    }

    /**
     * Deletes all files stored by this manager.
     *
     * Files that can't be deleted still don't throw an error. You have to rerun
     * {@link ImpedanceDataTableFileManager#wipe(Context)} in this case.
     *
     * @param context The application context.
     * @return        {@code true} if all files could be successfully deleted, {@code false} if not.
     */
    public static boolean wipe(Context context) {
        boolean success = true;
        for (File file : getFiles()) {
            if (!file.delete()) {
                Log.e("oxid", "Unable to delete '" + file.getName() + "', keeping file.");
                success = false;
            }
        }

        MediaScannerConnection.scanFile(
            context, new String[] {_filemanager.getDirectory().getAbsolutePath()}, null, null);

        return success;
    }

    private static String getFilenameFromTimestamp(Date date) {
        return date.toString() + ".csv";
    }

    private static double parseUnitString(String string, String unit) throws NumberFormatException {
        String float_regex = "(?:-|\\+)?\\d+(?:\\.\\d+)?";
        Pattern regex = Pattern.compile("(" + float_regex + ")(?:" + Pattern.quote(unit) + ")?");
        Matcher matcher = regex.matcher(string);

        if (matcher.matches()) {
            return Double.valueOf(matcher.group(1));
        }
        else {
            throw new NumberFormatException(
                "Can't parse unit string: " + string);
        }
    }

    private static String toString(Complex complex) {
        String glue_operator = complex.getImaginary() < 0 ? "" : "+";
        return complex.getReal() + glue_operator + complex.getImaginary() + "j";
    }

    private static FileManager _filemanager;
}
