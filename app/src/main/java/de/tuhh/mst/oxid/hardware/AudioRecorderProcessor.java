package de.tuhh.mst.oxid.hardware;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;


/**
 * Handles recording from the microphone.
 */
public class AudioRecorderProcessor {
    /**
     * Instantiates a new {@link AudioRecorderProcessor} without an
     * {@link OnSignalReceivedListener}.
     *
     * To start the recording with {@link #start()}, you need to call
     * {@link #setOnSignalReceivedListener(OnSignalReceivedListener)} before.
     *
     * @param sampling_rate The recording sample frequency.
     */
    public AudioRecorderProcessor(int sampling_rate) {
        this(sampling_rate, null);
    }

    /**
     * Instantiates a new {@link AudioRecorderProcessor}.
     *
     * @param sampling_rate The recording sample frequency.
     * @param listener      The {@link OnSignalReceivedListener} that processes the recorded signal.
     */
    public AudioRecorderProcessor(int sampling_rate, OnSignalReceivedListener listener) {
        this.sampling_rate = sampling_rate;
        _listener = listener;

        initializeAudioRecorder();
    }

    private void initializeAudioRecorder() {
        final int min_buffer_size = AudioRecord.getMinBufferSize(sampling_rate,
                                                                 AudioFormat.CHANNEL_IN_MONO,
                                                                 AudioFormat.ENCODING_PCM_16BIT);

        // For smooth recording, let's choose 4 buffer frames, where each buffer frame is
        // 4 * min_buffer_size.
        final int buffer_size = min_buffer_size * 16;
        final int notification_period_in_frames = buffer_size / 4;

        _recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                                    sampling_rate,
                                    AudioFormat.CHANNEL_IN_MONO,
                                    AudioFormat.ENCODING_PCM_16BIT,
                                    buffer_size);

        if (_recorder.getState() == AudioRecord.STATE_UNINITIALIZED) {
            throw new IllegalArgumentException(
                "Underlying recorder wasn't initialized successfully. Probably the sampling rate " +
                "is not supported by the device.");
        }

        _recorder.setRecordPositionUpdateListener(new AudioRecord.OnRecordPositionUpdateListener() {
            @Override
            public void onMarkerReached(AudioRecord recorder) {}

            @Override
            public void onPeriodicNotification(AudioRecord recorder) {
                short[] buffer = new short[notification_period_in_frames];
                recorder.read(buffer, 0, notification_period_in_frames);

                _listener.process_signal(buffer);
            }
        });

        int status_code = _recorder.setPositionNotificationPeriod(notification_period_in_frames);
        if (status_code != AudioRecord.SUCCESS) {
            throw new RuntimeException("Error while setting notification period.");
        }
    }

    /**
     * Sets the listener that receives buffer chunks with recorded signal data.
     *
     * You must set a listener before you start the recording via {@link #start()}.
     *
     * @param listener               The listener to set.
     * @throws IllegalStateException Thrown when currently recording.
     */
    public void setOnSignalReceivedListener(OnSignalReceivedListener listener) {
        if (_recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
            throw new IllegalStateException("Can't set listener while recording.");
        }

        _listener = listener;
    }

    /**
     * Gets the listener that receives buffer chunks with recorded signal data.
     *
     * If no listener is set, this function returns {@code null}.
     *
     * @return The listener currently set.
     */
    public OnSignalReceivedListener getOnSignalReceivedListener() {
        return _listener;
    }

    /**
     * Starts microphone recording and processing.
     *
     * You need to set an {@link OnSignalReceivedListener} before using
     * {@link #setOnSignalReceivedListener(OnSignalReceivedListener)}.
     *
     * @throws IllegalStateException Thrown when no {@link OnSignalReceivedListener} was set before
     *                               via
     *                               {@link #setOnSignalReceivedListener(OnSignalReceivedListener)}.
     */
    public void start() {
        if (_listener == null) {
            throw new IllegalStateException("No OnSignalReceivedListener set.");
        }

        // TODO What about input source? from headjack / general mic
        _recorder.startRecording();
    }

    /**
     * Stops the microphone input recording.
     *
     * Invoking {@link #start()} is possible after calling {@link #stop()}.
     */
    public void stop() {
        _recorder.stop();
    }

    /**
     * Releases the resources from the underlying {@link AudioRecord}.
     *
     * You should call this function when you are done with this class, there might occur system
     * problems when trying to use another {@link AudioRecorderProcessor} while there isn't
     * everything cleaned up.
     */
    public void release() {
        if (_recorder != null) {
            _recorder.release();
        }
    }

    /**
     * The sampling frequency of the recorder.
     */
    public final int sampling_rate;

    private AudioRecord _recorder;
    private OnSignalReceivedListener _listener;
}
