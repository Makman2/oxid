package de.tuhh.mst.oxid.ui.preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.EditTextPreference;
import android.text.InputType;
import android.util.AttributeSet;

import de.tuhh.mst.oxid.R;


/**
 * A setting that allows to type in a floating point value.
 *
 * This preference features the {@code %s}-feature, meaning inserting {@code %s} into the summary
 * will substitute for the current value.
 */
public class FloatPreference extends EditTextPreference {
    private final float MIN_VALUE_DEFAULT = Float.MIN_VALUE;
    private final float MAX_VALUE_DEFAULT = Float.MAX_VALUE;

    public FloatPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public FloatPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FloatPreference(Context context) {
        super(context);
        init(context, null);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray xml_attributes = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.FloatPreference, 0, 0);

            try {
                // Need to bypass setMinValue() on purpose, because _min_value and _max_value have
                // unknown state. The checks need to be avoided for a case where these two values
                // do conflict.
                _min_value = xml_attributes.getFloat(
                    R.styleable.FloatPreference_minFloatValue, MIN_VALUE_DEFAULT);
                setMaxValue(xml_attributes.getFloat(
                    R.styleable.FloatPreference_maxFloatValue, MAX_VALUE_DEFAULT));
            }
            finally {
                xml_attributes.recycle();
            }
        }
        else {
            // Also here a bypass of setMinValue() is needed. See above.
            _min_value = MIN_VALUE_DEFAULT;
            setMaxValue(MAX_VALUE_DEFAULT);
        }

        getEditText().setInputType(
            InputType.TYPE_CLASS_NUMBER |
            InputType.TYPE_NUMBER_FLAG_DECIMAL |
            InputType.TYPE_NUMBER_FLAG_SIGNED);
    }

    @Override
    public void setText(String text) {
        // Remove leading zeroes and clamp to range.
        float value;
        try {
            value = Float.parseFloat(text);
            value = Math.max(Math.min(value, _max_value), _min_value);
        }
        catch (NumberFormatException ex) {
            if (text.charAt(0) == '-') {
                // Number too small.
                value = _min_value;
            }
            else {
                // Number too large.
                value = _max_value;
            }
        }

        text = Float.toString(value);
        super.setText(text);
    }

    @Override
    public CharSequence getSummary() {
        return super.getSummary().toString().replace("%s", getText());
    }

    /**
     * Returns the value that limits the minimum input value.
     *
     * If user supplies a value less than this minimum, it will be clamped.
     *
     * @return The minimum value.
     */
    public float getMinValue() {
        return _min_value;
    }

    /**
     * Returns the value that limits the minimum input value.
     *
     * If user supplies a value less than this minimum, it will be clamped.
     *
     * @param value                     The new minimum value.
     * @throws IllegalArgumentException Thrown when value is less than 0 or greater than the value
     *                                  from {@link #getMaxValue()}.
     */
    public void setMinValue(float value) {
        if (value > _max_value) {
            throw new IllegalArgumentException("Minimum value can't be greater than maximum.");
        }

        _min_value = value;
    }

    /**
     * Returns the value that limits the maximum input value.
     *
     * If user supplies a value greater than this maximum, it will be clamped.
     *
     * @return The maximum value.
     */
    public float getMaxValue() {
        return _max_value;
    }

    /**
     * Defines the value that limits the maximum input value.
     *
     * If user supplies a value greater than this maximum, it will be clamped.
     *
     * @param value                     The new maximum.
     * @throws IllegalArgumentException Thrown when value is below the value from
     *                                  {@link #getMinValue()}.
     */
    public void setMaxValue(float value) {
        if (value < _min_value) {
            throw new IllegalArgumentException("Maximum value can't be less minimum.");
        }

        _max_value = value;
    }

    private float _min_value;
    private float _max_value;
}
