package de.tuhh.mst.oxid.hardware;

import android.test.AndroidTestCase;


class EmptyOnSignalReceivedListener implements OnSignalReceivedListener {
    @Override
    public void process_signal(short[] signal) {}
}


public class AudioRecorderProcessorTest extends AndroidTestCase {

    public void testSamplingRate() throws Exception {
        AudioRecorderProcessor uut;

        uut = new AudioRecorderProcessor(8000);
        assertEquals(8000, uut.sampling_rate);

        uut = new AudioRecorderProcessor(44100, null);
        assertEquals(44100, uut.sampling_rate);
    }

    public void testListener() throws Exception {
        AudioRecorderProcessor uut;

        uut = new AudioRecorderProcessor(8000);
        assertNull(uut.getOnSignalReceivedListener());

        OnSignalReceivedListener listener = new EmptyOnSignalReceivedListener();
        uut = new AudioRecorderProcessor(8000, listener);
        assertEquals(listener, uut.getOnSignalReceivedListener());

        uut.setOnSignalReceivedListener(null);
        assertNull(uut.getOnSignalReceivedListener());

        uut.setOnSignalReceivedListener(listener);
        assertEquals(listener, uut.getOnSignalReceivedListener());
    }
}
