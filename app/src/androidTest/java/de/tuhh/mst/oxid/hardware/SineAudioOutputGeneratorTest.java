package de.tuhh.mst.oxid.hardware;

import android.test.AndroidTestCase;


public class SineAudioOutputGeneratorTest extends AndroidTestCase {

    public void testFields() throws Exception {
        SineAudioOutputGenerator uut = new SineAudioOutputGenerator(12000, 7777.0);
        assertEquals(12000, uut.getFrequency());
        assertEquals(7777.0, uut.getAmplitude());
        assertEquals(0.0, uut.getOffset());

        uut = new SineAudioOutputGenerator(5000, 55.5, 1.1);
        assertEquals(5000, uut.getFrequency());
        assertEquals(55.5, uut.getAmplitude());
        assertEquals(1.1, uut.getOffset());
    }
}
