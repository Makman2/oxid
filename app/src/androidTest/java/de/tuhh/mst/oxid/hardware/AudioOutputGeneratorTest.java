package de.tuhh.mst.oxid.hardware;

import android.test.AndroidTestCase;


class EmptyOnSignalQueriedListener implements OnSignalQueriedListener {
    @Override
    public short[] get_signal() {
        return new short[0];
    }
}


public class AudioOutputGeneratorTest extends AndroidTestCase {

    public void testSamplingRate() throws Exception {
        AudioOutputGenerator uut;

        uut = new AudioOutputGenerator(8000);
        assertEquals(8000, uut.sampling_rate);

        uut = new AudioOutputGenerator(44100, null);
        assertEquals(44100, uut.sampling_rate);
    }

    public void testListener() throws Exception {
        AudioOutputGenerator uut;

        uut = new AudioOutputGenerator(8000);
        assertNull(uut.getOnSignalQueriedListener());

        OnSignalQueriedListener listener = new EmptyOnSignalQueriedListener();
        uut = new AudioOutputGenerator(8000, listener);
        assertEquals(listener, uut.getOnSignalQueriedListener());

        uut.setOnSignalQueriedListener(null);
        assertNull(uut.getOnSignalQueriedListener());

        uut.setOnSignalQueriedListener(listener);
        assertEquals(listener, uut.getOnSignalQueriedListener());
    }
}
