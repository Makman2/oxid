package de.tuhh.mst.oxid.hardware;

import android.test.AndroidTestCase;

import java.util.ArrayList;
import java.util.List;


class TestListener extends BufferedOnSignalReceivedListener {
    public TestListener(int buffer_size) {
        super(buffer_size);

        calls = new ArrayList<>();
    }

    @Override
    public void process_buffered_signal(short[] signal) {
        calls.add(signal);
    }

    public final List<short[]> calls;
}


public class BufferedOnSignalReceivedListenerTest extends AndroidTestCase {

    public void assertArraysEqual(short[] expected, short[] actual) {
        assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals("Arrays do not match at index " + i + ",", expected[i], actual[i]);
        }
    }

    public void testBufferNotFilling() throws Exception {
        TestListener uut = new TestListener(10);

        assertEquals(0, uut.calls.size());

        // Array length does not suffice to trigger a process_buffered_signal() call.
        uut.process_signal(new short[]{1, 2, 3, 4, 5});

        assertEquals(0, uut.calls.size());
    }

    public void testBufferFillsExactly() throws Exception {
        TestListener uut = new TestListener(10);

        assertEquals(0, uut.calls.size());

        // Array length does now fill up buffer exactly, this should trigger a
        // process_buffered_signal() call.
        uut.process_signal(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

        assertEquals(1, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                          uut.calls.get(0));

        // Divided into two chunks.
        uut.process_signal(new short[]{11, 12, 13});
        uut.process_signal(new short[]{14, 15, 16, 17, 18, 19, 20});

        assertEquals(2, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                          uut.calls.get(1));
    }

    public void testAsymmetricBufferOverflows() throws Exception {
        TestListener uut = new TestListener(10);

        assertEquals(0, uut.calls.size());

        uut.process_signal(new short[]{1, 2, 3, 4, 5, 6, 7});

        assertEquals(0, uut.calls.size());

        uut.process_signal(new short[]{8, 9, 10, 11, 12, 13, 14});

        assertEquals(1, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                          uut.calls.get(0));

        uut.process_signal(new short[]{15, 16, 17, 18, 19, 20, 21, 22});

        assertEquals(2, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                          uut.calls.get(1));
    }

    public void testBufferOverflows2x() throws Exception {
        TestListener uut = new TestListener(5);

        assertEquals(0, uut.calls.size());

        uut.process_signal(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

        assertEquals(2, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{6, 7, 8, 9, 10},
                          uut.calls.get(1));

        // Asymmetric overflow.
        uut.process_signal(new short[]{1, 2});

        assertEquals(2, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{6, 7, 8, 9, 10},
                          uut.calls.get(1));

        uut.process_signal(new short[]{11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22});

        assertEquals(4, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{6, 7, 8, 9, 10},
                          uut.calls.get(1));
        assertArraysEqual(new short[]{1, 2, 11, 12, 13},
                          uut.calls.get(2));
        assertArraysEqual(new short[]{14, 15, 16, 17, 18},
                          uut.calls.get(3));

        uut.process_signal(new short[]{30, 31});

        assertEquals(5, uut.calls.size());
        assertArraysEqual(new short[]{1, 2, 3, 4, 5},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{6, 7, 8, 9, 10},
                          uut.calls.get(1));
        assertArraysEqual(new short[]{1, 2, 11, 12, 13},
                          uut.calls.get(2));
        assertArraysEqual(new short[]{14, 15, 16, 17, 18},
                          uut.calls.get(3));
        assertArraysEqual(new short[]{19, 20, 21, 22, 30},
                          uut.calls.get(4));
    }

    public void testBufferOverflows10x() throws Exception {
        TestListener uut = new TestListener(3);

        assertEquals(0, uut.calls.size());

        uut.process_signal(new short[]{100});

        assertEquals(0, uut.calls.size());

        uut.process_signal(new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
                                       18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31});

        assertEquals(10, uut.calls.size());
        assertArraysEqual(new short[]{100, 1, 2},
                          uut.calls.get(0));
        assertArraysEqual(new short[]{3, 4, 5},
                          uut.calls.get(1));
        assertArraysEqual(new short[]{6, 7, 8},
                          uut.calls.get(2));
        assertArraysEqual(new short[]{9, 10, 11},
                          uut.calls.get(3));
        assertArraysEqual(new short[]{12, 13, 14},
                          uut.calls.get(4));
        assertArraysEqual(new short[]{15, 16, 17},
                          uut.calls.get(5));
        assertArraysEqual(new short[]{18, 19, 20},
                          uut.calls.get(6));
        assertArraysEqual(new short[]{21, 22, 23},
                          uut.calls.get(7));
        assertArraysEqual(new short[]{24, 25, 26},
                          uut.calls.get(8));
        assertArraysEqual(new short[]{27, 28, 29},
                          uut.calls.get(9));
    }
}
