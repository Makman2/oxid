package de.tuhh.mst.oxid.math.estimators;

import android.test.AndroidTestCase;


public class IdentityEstimatorTest extends AndroidTestCase {
    public void testConstruction() throws Exception {
        new IdentityEstimator();
    }

    public void testInitialEstimation() throws Exception {
        IdentityEstimator uut = new IdentityEstimator();

        assertEquals(0.0, uut.estimate());
    }

    public void testEstimation() throws Exception {
        IdentityEstimator uut = new IdentityEstimator();

        uut.put(10.0);
        assertEquals(10.0, uut.estimate());
        uut.put(-10.0);
        assertEquals(-10.0, uut.estimate());
        uut.put(33.0);
        assertEquals(33.0, uut.estimate());
        uut.put(55.81);
        assertEquals(55.81, uut.estimate());
    }

    public void testReset() throws Exception {
        IdentityEstimator uut = new IdentityEstimator();

        uut.put(20.0);
        assertEquals(20.0, uut.estimate());

        uut.reset();
        assertEquals(0.0, uut.estimate());

        uut.put(40.0);
        assertEquals(40.0, uut.estimate());
    }
}
