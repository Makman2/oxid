package de.tuhh.mst.oxid.utilities;

import android.test.AndroidTestCase;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.util.OpenIntToDoubleHashMap;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;


public class mathTest extends AndroidTestCase {
    public void testLog2() throws Exception {
        assertEquals(0, math.log2(1));
        assertEquals(1, math.log2(2));
        assertEquals(1, math.log2(3));
        assertEquals(2, math.log2(4));
        assertEquals(2, math.log2(5));
        assertEquals(2, math.log2(6));
        assertEquals(2, math.log2(7));
        assertEquals(3, math.log2(8));
        assertEquals(4, math.log2(16));
        assertEquals(5, math.log2(32));
        assertEquals(6, math.log2(64));
        assertEquals(6, math.log2(100));
        assertEquals(10, math.log2(1024));

        boolean thrown = false;
        try {
            math.log2(0);
        }
        catch (IllegalArgumentException ex) {
            thrown = true;
        }
        assertTrue("IllegalArgumentException not thrown.", thrown);

        thrown = false;
        try {
            math.log2(-10);
        }
        catch (IllegalArgumentException ex) {
            thrown = true;
        }
        assertTrue("IllegalArgumentException not thrown.", thrown);
    }

    public void testPow() throws Exception {
        assertEquals(0, math.pow(0, 10));
        assertEquals(1, math.pow(1, 10));
        assertEquals(1, math.pow(10, 0));

        assertEquals(333, math.pow(333, 1));
        assertEquals(256, math.pow(2, 8));
        assertEquals(729, math.pow(9, 3));
        assertEquals(100000, math.pow(10, 5));
    }

    public void testDFT() throws Exception {
        double[] signal = {0.0, 4.0, 0.0, -4.0};
        double[] expected_imag = {0.0, -8.0, 0.0, 8.0};
        Complex[] real = math.dft(signal);

        assertEquals(expected_imag.length, real.length);
        for (int i = 0; i < expected_imag.length; i++) {
            assertEquals("i=" + i + ":", 0.0, real[i].getReal());
            assertEquals("i=" + i + ":", expected_imag[i], real[i].getImaginary());
        }

        signal = new double[] {1.0, 6.0, 1.0, -4.0, 1.0, 6.0};
        double[] expected_abs = {11.0, 3.3679586919241777, 16.0312195418814, 4.759921664218056,
                                 5.0, 4.759921664218055, 16.0312195418814, 3.367958691924178};
        DoubleBuffer real_abs = DoubleBuffer.allocate(expected_abs.length);
        real = math.dft(signal);
        for (Complex value : real) {
            real_abs.put(value.abs());
        }

        assertEquals(expected_abs.length, real.length);
        for (int i = 0; i < expected_abs.length; i++) {
            assertEquals("i=" + i + ":", expected_abs[i], real_abs.get(i));
        }
    }

    public void testRFFT() throws Exception {
        double[] signal = {0.0, 4.0, 0.0, -4.0};
        double[] expected_real = {0.0, 0.0, 0.0};
        double[] expected_imag = {0.0, -4.0, 0.0};

        Complex[] actual = math.rfft(signal);

        assertEquals(expected_imag.length, actual.length);
        assertEquals(expected_real.length, actual.length);
        for (int i = 0; i < expected_imag.length; i++) {
            assertEquals("i=" + i + ":", expected_real[i], actual[i].getReal());
            assertEquals("i=" + i + ":", expected_imag[i], actual[i].getImaginary());
        }

        signal = new double[] {1.0, 6.0, 1.0, -4.0, 1.0, 6.0};
        double[] expected_abs =
            {1.375, 0.8419896729810444, 4.00780488547035, 1.189980416054514, 0.625};

        DoubleBuffer actual_abs = DoubleBuffer.allocate(expected_abs.length);
        actual = math.rfft(signal);
        for (Complex value : actual) {
            actual_abs.put(value.abs());
        }

        assertEquals(expected_abs.length, actual.length);
        for (int i = 0; i < expected_abs.length; i++) {
            assertEquals("i=" + i + ":", expected_abs[i], actual_abs.get(i));
        }
    }

    public void testClamp() throws Exception {
        assertEquals(3, math.clamp(3, 0, 5));
        assertEquals(0, math.clamp(0, 0, 5));
        assertEquals(0, math.clamp(-1, 0, 5));
        assertEquals(5, math.clamp(5, 0, 5));
        assertEquals(5, math.clamp(6, 0, 5));
    }

    public void testComputeGaussSmoothKernel() throws Exception {
        OpenIntToDoubleHashMap kernel = math.compute_gauss_smooth_kernel(1);
        assertEquals(7, kernel.size());
        assertEquals(0.0044318484119380075, kernel.get(-3));
        assertEquals(0.05399096651318806, kernel.get(-2));
        assertEquals(0.24197072451914337, kernel.get(-1));
        assertEquals(0.3989422804014327, kernel.get(0));
        assertEquals(0.24197072451914337, kernel.get(1));
        assertEquals(0.05399096651318806, kernel.get(2));
        assertEquals(0.0044318484119380075, kernel.get(3));

        kernel = math.compute_gauss_smooth_kernel(0.5);
        assertEquals(3, kernel.size());
        assertEquals(0.10798193302637613, kernel.get(-1));
        assertEquals(0.7978845608028654, kernel.get(0));
        assertEquals(0.10798193302637613, kernel.get(1));

        kernel = math.compute_gauss_smooth_kernel(1, 0.2);
        assertEquals(3, kernel.size());
        assertEquals(0.24197072451914337, kernel.get(-1));
        assertEquals(0.3989422804014327, kernel.get(0));
        assertEquals(0.24197072451914337, kernel.get(1));

        kernel = math.compute_gauss_smooth_kernel(1, 1);
        assertEquals(1, kernel.size());
        assertEquals(0.3989422804014327, kernel.get(0));
    }

    public void testComputeIdentitySmoothKernel() throws Exception {
        OpenIntToDoubleHashMap kernel = math.compute_identity_smooth_kernel();
        assertEquals(1, kernel.size());
        assertEquals(1.0, kernel.get(0));
    }

    public void testComputeAverageSmoothKernel() throws Exception {
        OpenIntToDoubleHashMap kernel = math.compute_average_smooth_kernel(0);
        assertEquals(1, kernel.size());
        assertEquals(1.0, kernel.get(0));

        kernel = math.compute_average_smooth_kernel(1);
        assertEquals(3, kernel.size());
        assertEquals(1.0 / 3.0, kernel.get(-1));
        assertEquals(1.0 / 3.0, kernel.get(0));
        assertEquals(1.0 / 3.0, kernel.get(1));

        kernel = math.compute_average_smooth_kernel(2);
        assertEquals(5, kernel.size());
        assertEquals(0.2, kernel.get(-2));
        assertEquals(0.2, kernel.get(-1));
        assertEquals(0.2, kernel.get(0));
        assertEquals(0.2, kernel.get(1));
        assertEquals(0.2, kernel.get(2));
    }

    public void testSmooth() throws Exception {
        double[] data = {1.0, 2.0, 3.0, 0.0, 0.0, 1.0, 7.0, 10.0, -3.0, 2.0, -1.0};

        // Asymmetric kernel.
        OpenIntToDoubleHashMap kernel = new OpenIntToDoubleHashMap();
        kernel.put(-1, 0.0);
        kernel.put(0, 0.5);
        kernel.put(1, 0.25);
        kernel.put(2, 0.25);

        double[] expected = {1.75, 1.75, 1.5, 0.25, 2.0, 4.75, 5.25, 4.75, -1.25, 0.5, -1.0};

        double[] smoothed = math.smooth(data, kernel);
        assertEquals(expected.length, smoothed.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals("i=" + i + ":", expected[i], smoothed[i]);
        }

        // Simple averaging kernel.
        kernel = math.compute_average_smooth_kernel(2);

        expected = new double[] {1.6, 1.4, 1.2000000000000002, 1.2000000000000002, 2.2, 3.6,
                                 3.0000000000000004, 3.4, 3.0, 1.4, -0.8000000000000002};

        smoothed = math.smooth(data, kernel);
        assertEquals(expected.length, smoothed.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals("i=" + i + ":", expected[i], smoothed[i]);
        }

        // Test case when kernel is larger than data.
        data = new double[]{1.0, 2.0, 3.0};
        kernel = math.compute_average_smooth_kernel(2);

        expected = new double[] {1.6, 2.0, 2.4000000000000004};

        smoothed = math.smooth(data, kernel);
        assertEquals(expected.length, smoothed.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals("i=" + i + ":", expected[i], smoothed[i]);
        }
    }

    public void testDetectMaxima() throws Exception {
        double[] data = {0.0, -1.0, 2.0, 4.0, 5.0, 3.0, 2.5, 4.5, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0};
        int[] expected = {4, 7, 10};

        List<Integer> real = math.detect_maxima(data);

        assertEquals(expected.length, real.size());
        for (int i = 0; i < expected.length; i++) {
            assertEquals("i=" + i + ":", expected[i], (int)real.get(i));
        }
    }

    public void testDetectGlobalMaximum() throws Exception {
        double[] data = {};
        assertEquals(-1, math.detect_global_maximum(data));

        data = new double[] {-10.0};
        assertEquals(0, math.detect_global_maximum(data));

        data = new double[] {-10.0, 10.0};
        assertEquals(1, math.detect_global_maximum(data));

        data = new double[] {3.0, 3.4, 3.2, 3.5, -2.0};
        assertEquals(3, math.detect_global_maximum(data));
    }

    public void testPick() throws Exception {
        double[] data = {0.0, -1.0, 2.0, 4.0, 5.0, 3.0, 2.5, 4.5, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0};

        ArrayList<Integer> indices = new ArrayList<>();

        double[] real = math.pick(indices, data);
        assertEquals(0, real.length);

        indices.add(3);
        indices.add(6);
        indices.add(0);
        indices.add(2);
        double[] expected = {4.0, 2.5, 0.0, 2.0};

        real = math.pick(indices, data);
        assertEquals(expected.length, real.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals("i=" + i + ":", expected[i], real[i]);
        }
    }

    public void testSum() throws Exception {
        double[] data = {0.0, -1.0, 2.0, 4.0, 5.0, 3.0, 2.5, 4.5, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0};

        // Sum up complete series.
        assertEquals(49.0, math.sum(data));
        // Sum up parts.
        assertEquals(1.0, math.sum(data, 0, 3));
        assertEquals(11.0, math.sum(data, 2, 5));
        assertEquals(23.0, math.sum(data, 8, 13));
        // Sum up zero or negative range.
        assertEquals(0.0, math.sum(data, 0, 0));
        assertEquals(0.0, math.sum(data, 7, 7));
        assertEquals(0.0, math.sum(data, 1, 0));
        assertEquals(0.0, math.sum(data, 7, 3));
        // Sum up with range 1.
        assertEquals(0.0, math.sum(data, 0, 1));
        assertEquals(6.0, math.sum(data, 13, 14));
    }

    public void testSumComplex() throws Exception {
        Complex[] data = {new Complex(0.0, 0.0), new Complex(-1.0, 0.0), new Complex(0.0, 3.0),
                          new Complex(2.0, 1.0), new Complex(-5.0, 3.0), new Complex(2.0, -1.0),
                          new Complex(1.0, 2.0), new Complex(-1.0, -1.0), new Complex(5.0, -6.0)};

        // Sum up complete series.
        assertEquals(new Complex(3.0, 1.0), math.sum(data));
        // Sum up parts.
        assertEquals(new Complex(-1.0, 3.0), math.sum(data, 0, 3));
        assertEquals(new Complex(-3.0, 7.0), math.sum(data, 2, 5));
        assertEquals(new Complex(5.0, -5.0), math.sum(data, 6, 9));
        // Sum up zero or negative range.
        assertEquals(new Complex(0.0, 0.0), math.sum(data, 0, 0));
        assertEquals(new Complex(0.0, 0.0), math.sum(data, 7, 7));
        assertEquals(new Complex(0.0, 0.0), math.sum(data, 1, 0));
        assertEquals(new Complex(0.0, 0.0), math.sum(data, 7, 3));
        // Sum up with range 1.
        assertEquals(new Complex(0.0, 0.0), math.sum(data, 0, 1));
        assertEquals(new Complex(5.0, -6.0), math.sum(data, 8, 9));
    }

    public void testMean() throws Exception {
        double[] data = {1.0, 2.0, 3.0, 4.0, 5.0, 2.5, 5.5, 10.0};
        assertEquals(4.125, math.mean(data));

        data = new double[] {-5.0, -3.0, 1.0, 22.0};
        assertEquals(3.75, math.mean(data));
    }
}
