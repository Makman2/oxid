package de.tuhh.mst.oxid.hardware;

import android.test.AndroidTestCase;


public class StaticAudioOutputGeneratorTest extends AndroidTestCase {

    public void testEmptySample() throws Exception {
        new StaticAudioOutputGenerator(8000, new short[4]);

        try {
            new StaticAudioOutputGenerator(8000, new short[0]);
            fail("Expected IllegalArgumentException.");
        }
        catch (IllegalArgumentException e) {}
    }

    public void testSamplingRate() throws Exception {
        StaticAudioOutputGenerator uut = new StaticAudioOutputGenerator(8000, new short[4]);
        assertEquals(8000, uut.sampling_rate);
    }
}
