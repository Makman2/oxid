package de.tuhh.mst.oxid.plotting;

import android.test.AndroidTestCase;

import java.util.ArrayList;


public class EquidistantYSeriesTest extends AndroidTestCase {
    private ArrayList<Double> _data;

    @Override
    protected void setUp() throws Exception {
        _data = new ArrayList<>();
        _data.add(3.0);
        _data.add(-2.0);
        _data.add(5.0);
        _data.add(4.0);
        _data.add(1.0);
    }

    public void testSize() throws Exception {
        EquidistantYSeries uut = new EquidistantYSeries(_data, "");
        assertEquals(_data.size(), uut.size());
    }

    public void testTitle() throws Exception {
        EquidistantYSeries uut = new EquidistantYSeries(_data, "title");
        assertEquals("title", uut.getTitle());
    }

    public void testX() throws Exception {
        EquidistantYSeries uut = new EquidistantYSeries(_data, "");

        double[] expected = {0.0, 1.0, 2.0, 3.0, 4.0};
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], uut.getX(i));
        }

        uut = new EquidistantYSeries(_data, 4.0, "");
        expected = new double[] {0.0, 4.0, 8.0, 12.0, 16.0};
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], uut.getX(i));
        }

        uut = new EquidistantYSeries(_data, -10.0, 0.5, "");
        expected = new double[] {-10.0, -9.5, -9.0, -8.5, -8.0};
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], uut.getX(i));
        }
    }

    public void testY() throws Exception {
        EquidistantYSeries uut = new EquidistantYSeries(_data, -55.0, 100.0, "");

        for (int i = 0; i < _data.size(); i++) {
            assertEquals(_data.get(i), uut.getY(i));
        }

        assertEquals(_data, uut.getYValues());
    }
}
