package de.tuhh.mst.oxid.utilities;

import android.test.AndroidTestCase;

import org.apache.commons.math3.util.Pair;


public class unitsTest extends AndroidTestCase {
    public void testMetricPrefixOrdering() throws Exception {
        assertTrue(units.MetricPrefix.FEMTO.compareTo(units.MetricPrefix.PICO) < 0);
        assertTrue(units.MetricPrefix.PICO.compareTo(units.MetricPrefix.NANO) < 0);
        assertTrue(units.MetricPrefix.NANO.compareTo(units.MetricPrefix.MICRO) < 0);
        assertTrue(units.MetricPrefix.MICRO.compareTo(units.MetricPrefix.MILLI) < 0);
        assertTrue(units.MetricPrefix.MILLI.compareTo(units.MetricPrefix.NONE) < 0);
        assertTrue(units.MetricPrefix.NONE.compareTo(units.MetricPrefix.KILO) < 0);
        assertTrue(units.MetricPrefix.KILO.compareTo(units.MetricPrefix.MEGA) < 0);
        assertTrue(units.MetricPrefix.MEGA.compareTo(units.MetricPrefix.GIGA) < 0);
        assertTrue(units.MetricPrefix.GIGA.compareTo(units.MetricPrefix.TERRA) < 0);
    }

    public void testMetricPrefixSymbolStrings() throws Exception {
        assertEquals("f", units.MetricPrefix.FEMTO.toSymbolString());
        assertEquals("p", units.MetricPrefix.PICO.toSymbolString());
        assertEquals("n", units.MetricPrefix.NANO.toSymbolString());
        assertEquals("μ", units.MetricPrefix.MICRO.toSymbolString());
        assertEquals("m", units.MetricPrefix.MILLI.toSymbolString());
        assertEquals("", units.MetricPrefix.NONE.toSymbolString());
        assertEquals("k", units.MetricPrefix.KILO.toSymbolString());
        assertEquals("M", units.MetricPrefix.MEGA.toSymbolString());
        assertEquals("G", units.MetricPrefix.GIGA.toSymbolString());
        assertEquals("T", units.MetricPrefix.TERRA.toSymbolString());
    }

    public void testMetricPrefixMultipliers() throws Exception {
        assertEquals(1E-15, units.MetricPrefix.FEMTO.getMultiplier());
        assertEquals(1E-12, units.MetricPrefix.PICO.getMultiplier());
        assertEquals(1E-9, units.MetricPrefix.NANO.getMultiplier());
        assertEquals(1E-6, units.MetricPrefix.MICRO.getMultiplier());
        assertEquals(1E-3, units.MetricPrefix.MILLI.getMultiplier());
        assertEquals(1E+0, units.MetricPrefix.NONE.getMultiplier());
        assertEquals(1E+3, units.MetricPrefix.KILO.getMultiplier());
        assertEquals(1E+6, units.MetricPrefix.MEGA.getMultiplier());
        assertEquals(1E+9, units.MetricPrefix.GIGA.getMultiplier());
        assertEquals(1E+12, units.MetricPrefix.TERRA.getMultiplier());
    }

    public void testConvertToMetricPrefix() throws Exception {
        assertEquals(50.0, units.convertToMetricPrefix(50E+12, units.MetricPrefix.TERRA));
        assertEquals(22.0, units.convertToMetricPrefix(22000, units.MetricPrefix.KILO));
        assertEquals(47.0, units.convertToMetricPrefix(0.047, units.MetricPrefix.MILLI));
        assertEquals(0.03, units.convertToMetricPrefix(0.00003, units.MetricPrefix.MILLI));
        assertEquals(2.3, units.convertToMetricPrefix(2.3E-15, units.MetricPrefix.FEMTO));
    }

    public void testConvertToNearestMetricPrefix() throws Exception {
        Pair<Double, units.MetricPrefix> result;

        result = units.convertToNearestMetricPrefix(1E-16);
        assertEquals(0.09999999999999999, result.getKey());
        assertEquals(units.MetricPrefix.FEMTO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-15);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.FEMTO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-13);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.FEMTO, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E-13);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.FEMTO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-12);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.PICO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-10);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.PICO, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E-10);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.PICO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-9);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.NANO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-7);
        assertEquals(99.99999999999999, result.getKey());
        assertEquals(units.MetricPrefix.NANO, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E-7);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.NANO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-6);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.MICRO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-4);
        assertEquals(100.00000000000001, result.getKey());
        assertEquals(units.MetricPrefix.MICRO, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E-4);
        assertEquals(999.0000000000001, result.getKey());
        assertEquals(units.MetricPrefix.MICRO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-3);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.MILLI, result.getValue());

        result = units.convertToNearestMetricPrefix(1E-1);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.MILLI, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E-1);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.MILLI, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+0);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.NONE, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+2);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.NONE, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E+2);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.NONE, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+3);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.KILO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+5);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.KILO, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E+5);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.KILO, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+6);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.MEGA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+8);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.MEGA, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E+8);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.MEGA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+9);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.GIGA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+11);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.GIGA, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E+11);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.GIGA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+12);
        assertEquals(1.0, result.getKey());
        assertEquals(units.MetricPrefix.TERRA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+14);
        assertEquals(100.0, result.getKey());
        assertEquals(units.MetricPrefix.TERRA, result.getValue());

        result = units.convertToNearestMetricPrefix(9.99E+14);
        assertEquals(999.0, result.getKey());
        assertEquals(units.MetricPrefix.TERRA, result.getValue());

        result = units.convertToNearestMetricPrefix(1E+15);
        assertEquals(1000.0, result.getKey());
        assertEquals(units.MetricPrefix.TERRA, result.getValue());

        result = units.convertToNearestMetricPrefix(4.27E+16);
        assertEquals(42700.0, result.getKey());
        assertEquals(units.MetricPrefix.TERRA, result.getValue());
    }

    public void testFormatUnitString() throws Exception {
        assertEquals("1000S", units.formatUnitString(1000, 0, units.MetricPrefix.NONE, "S"));
        assertEquals("1000S", units.formatUnitString(1000, units.MetricPrefix.NONE, "S"));
        assertEquals("1kS", units.formatUnitString(1000, 0, "S"));
        assertEquals("1kS", units.formatUnitString(1000, "S"));

        assertEquals("37.00mV", units.formatUnitString(0.037, 2, units.MetricPrefix.MILLI, "V"));
        assertEquals("37000μV", units.formatUnitString(0.037, units.MetricPrefix.MICRO, "V"));
        assertEquals("37mV", units.formatUnitString(0.037, 0, "V"));
        assertEquals("37mV", units.formatUnitString(0.037, "V"));

        assertEquals("0.0050MS", units.formatUnitString(5000, 4, units.MetricPrefix.MEGA, "S"));
        assertEquals("5000S", units.formatUnitString(5000, units.MetricPrefix.NONE, "S"));
        assertEquals("5.0kS", units.formatUnitString(5000, 1, "S"));
        assertEquals("5kS", units.formatUnitString(5000, "S"));

        assertEquals("0.04V", units.formatUnitString(0.0374, 2, units.MetricPrefix.NONE, "V"));
        assertEquals("37400μV", units.formatUnitString(0.0374, units.MetricPrefix.MICRO, "V"));
        assertEquals("37.4mV", units.formatUnitString(0.0374, 1, "V"));
        assertEquals("37mV", units.formatUnitString(0.0374, "V"));

        assertEquals("1.1kHz", units.formatUnitString(1111, 1, units.MetricPrefix.KILO, "Hz"));
        assertEquals("1111Hz", units.formatUnitString(1111, units.MetricPrefix.NONE, "Hz"));
        assertEquals("1.111kHz", units.formatUnitString(1111, 3, "Hz"));
        assertEquals("1kHz", units.formatUnitString(1111, "Hz"));
    }
}
