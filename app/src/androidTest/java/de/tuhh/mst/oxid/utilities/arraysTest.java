package de.tuhh.mst.oxid.utilities;

import android.test.AndroidTestCase;


public class arraysTest extends AndroidTestCase {
    public void testToDoubleFromShort() throws Exception {
        short[] input = {1, 2, 3, 10, -50, 66, 0};
        double[] expected = {1.0, 2.0, 3.0, 10.0, -50.0, 66.0, 0.0};

        double[] real = arrays.toDouble(input);

        assertEquals(expected.length, real.length);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], real[i]);
        }
    }
}
