package de.tuhh.mst.oxid.math.estimators;

import android.test.AndroidTestCase;


public class RelativeCutoffEstimatorTest extends AndroidTestCase {
    public void testConstruction() throws Exception {
        MeanEstimator base_estimator = new MeanEstimator();

        RelativeCutoffEstimator uut = new RelativeCutoffEstimator(base_estimator);
        assertEquals(base_estimator, uut.getBaseEstimator());
        assertEquals(0.1, uut.getRelativeDeviation());

        uut = new RelativeCutoffEstimator(base_estimator, 0.01);
        assertEquals(base_estimator, uut.getBaseEstimator());
        assertEquals(0.01, uut.getRelativeDeviation());
    }

    public void testBaseEstimation() throws Exception {
        MeanEstimator base_estimator = new MeanEstimator(4);
        RelativeCutoffEstimator uut = new RelativeCutoffEstimator(base_estimator);

        uut.put(100.0);
        assertEquals(100.0, uut.estimate());
        uut.put(100.0);
        assertEquals(100.0, uut.estimate());
        uut.put(103.0);
        assertEquals(101.0, uut.estimate());
        uut.put(97.0);
        assertEquals(100.0, uut.estimate());
    }

    public void testCutoff() throws Exception {
        MeanEstimator base_estimator = new MeanEstimator(4);
        RelativeCutoffEstimator uut = new RelativeCutoffEstimator(base_estimator);

        uut.put(100.0);
        assertEquals(100.0, uut.estimate());
        uut.put(100.0);
        assertEquals(100.0, uut.estimate());
        uut.put(103.0);
        assertEquals(101.0, uut.estimate());

        uut.put(200.0);
        assertEquals(200.0, uut.estimate());
        uut.put(202.0);
        assertEquals(201.0, uut.estimate());

        uut.put(300.0);
        assertEquals(300.0, uut.estimate());

        uut.put(330.1);
        assertEquals(330.1, uut.estimate());

        uut.put(100.0);
        assertEquals(100.0, uut.estimate());

        uut.put(89.9);
        assertEquals(89.9, uut.estimate());
    }

    public void testCutoffNegativeValues() throws Exception {
        MeanEstimator base_estimator = new MeanEstimator(4);
        RelativeCutoffEstimator uut = new RelativeCutoffEstimator(base_estimator);

        uut.put(-100.0);
        assertEquals(-100.0, uut.estimate());
        uut.put(-100.0);
        assertEquals(-100.0, uut.estimate());
        uut.put(-103.0);
        assertEquals(-101.0, uut.estimate());

        uut.put(-200.0);
        assertEquals(-200.0, uut.estimate());
        uut.put(-202.0);
        assertEquals(-201.0, uut.estimate());

        uut.put(-300.0);
        assertEquals(-300.0, uut.estimate());

        uut.put(-330.1);
        assertEquals(-330.1, uut.estimate());

        uut.put(-100.0);
        assertEquals(-100.0, uut.estimate());

        uut.put(-89.9);
        assertEquals(-89.9, uut.estimate());
    }

    public void testReset() throws Exception {
        MeanEstimator base_estimator = new MeanEstimator(4);
        RelativeCutoffEstimator uut = new RelativeCutoffEstimator(base_estimator);

        uut.put(100.0);
        uut.put(104.0);
        assertEquals(102.0, uut.estimate());

        uut.reset();
        uut.put(500.0);
        assertEquals(500.0, uut.estimate());
    }
}
