package de.tuhh.mst.oxid.math.estimators;

import android.test.AndroidTestCase;


public class MeanEstimatorTest extends AndroidTestCase {
    public void testConstruction() throws Exception {
        MeanEstimator uut = new MeanEstimator();
        assertEquals(10, uut.getSampleCount());

        uut = new MeanEstimator(44);
        assertEquals(44, uut.getSampleCount());
    }

    public void testEstimationUntilBufferFull() throws Exception {
        MeanEstimator uut = new MeanEstimator(4);

        // Undefined behaviour when doing an estimate now, so ignore that case.

        uut.put(100.0);
        assertEquals(100.0, uut.estimate());

        uut.put(200.0);
        assertEquals(150.0, uut.estimate());

        uut.put(300.0);
        assertEquals(200.0, uut.estimate());

        uut.put(400.0);
        assertEquals(250.0, uut.estimate());
    }

    public void testEstimationWithBufferFull() throws Exception {
        MeanEstimator uut = new MeanEstimator(4);

        uut.put(100.0);
        uut.put(200.0);
        uut.put(300.0);
        uut.put(400.0);
        assertEquals(250.0, uut.estimate());

        uut.put(100.0);
        assertEquals(250.0, uut.estimate());

        uut.put(1200.0);
        assertEquals(500.0, uut.estimate());

        uut.put(-1900.0);
        assertEquals(-50.0, uut.estimate());
    }

    public void testReset() throws Exception {
        MeanEstimator uut = new MeanEstimator(3);

        // This should work.
        uut.reset();

        uut.put(100.0);
        assertEquals(100.0, uut.estimate());

        uut.reset();
        uut.put(200.0);
        assertEquals(200.0, uut.estimate());

        uut.reset();
        uut.put(100.0);
        uut.put(200.0);
        assertEquals(150.0, uut.estimate());

        uut.reset();
        uut.put(100.0);
        uut.put(200.0);
        uut.put(300.0);
        assertEquals(200.0, uut.estimate());

        uut.reset();
        uut.put(100.0);
        uut.put(200.0);
        uut.put(300.0);
        uut.put(400.0);
        assertEquals(300.0, uut.estimate());
    }

    public void testGetCurrentSampleCount() throws Exception {
        MeanEstimator uut = new MeanEstimator(4);

        assertEquals(0, uut.getCurrentSampleCount());
        uut.put(100.0);
        assertEquals(1, uut.getCurrentSampleCount());
        uut.put(200.0);
        assertEquals(2, uut.getCurrentSampleCount());
        uut.put(300.0);
        assertEquals(3, uut.getCurrentSampleCount());
        uut.put(400.0);
        assertEquals(4, uut.getCurrentSampleCount());
        uut.put(500.0);
        assertEquals(4, uut.getCurrentSampleCount());
        uut.put(600.0);

        uut.reset();
        assertEquals(0, uut.getCurrentSampleCount());
    }
}
